import React, {useContext, useEffect, useState} from 'react';
import {DefaultTheme, NavigationContainer} from '@react-navigation/native';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {createStackNavigator} from '@react-navigation/stack';

// Components
import App from './App';
import LoginScreen from './screens/LoginScreen';
import SignupScreen from './screens/SignupScreen';

// Contexts
import {
  AuthContext,
  AuthContextProvider,
  ErrorContextProvider,
} from './contexts';

import FlashMessage from 'react-native-flash-message';

const AppWrapper = () => {
  return (
    <>
      <ErrorContextProvider>
        <AuthContextProvider>
          <SafeAreaProvider>
            <AppScreens />
          </SafeAreaProvider>
        </AuthContextProvider>
      </ErrorContextProvider>
      <FlashMessage position="top" />
    </>
  );
};

const AppScreens = () => {
  const Stack = createStackNavigator();
  const {isLoggedIn} = useContext(AuthContext);
  const [initRoute, setInitRoute] = useState<string>('Login');

  const navTheme = DefaultTheme;
  navTheme.colors.background = '#ffffff';

  useEffect(() => {
    if (isLoggedIn) {
      setInitRoute('App');
    }
  }, [isLoggedIn]);

  return (
    <>
      <NavigationContainer theme={navTheme}>
        <Stack.Navigator initialRouteName={initRoute}>
          {isLoggedIn ? (
            <Stack.Screen
              name="App"
              component={App}
              options={{headerShown: false}}
            />
          ) : (
            <>
              <Stack.Screen name="Login" component={LoginScreen} />
              <Stack.Screen name="Signup" component={SignupScreen} />
            </>
          )}
        </Stack.Navigator>
      </NavigationContainer>
    </>
  );
};

export default AppWrapper;
