import TUser from "./TUser";

type TPost = {
  _id: string;
  community: {leader: string};
  title: string;
  content: string;
  author: string;
  post_image: string | undefined;
  users_that_liked: Array<string>;
  comments: Array<{content: string; author: TUser}>;
  flags: Array<{
    _id: string;
    flag: EPostFlag;
    reason: string;
    flagged_by: string;
    flagged_date: Date;
    votes: Array<{_id: string; vote_by: string}>;
  }>;
};

export enum EPostFlag {
  inappropriate = 'inappropriate',
}

export default TPost;
