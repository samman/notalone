import TUser from './TUser';

type TCommunity = {
  _id: string;
  name: string;
  description: string;
  privacy: string;
  leader: TUser;
  is_member: boolean;
  community_logo: string | undefined;
};

export default TCommunity;
