type TUser = {
  _id: string;
  name: string;
  username: string;
  password: string;
  passwordConfirm: string;
  email: string;
  address: string;
  gender: Gender;
  profilePic: string;
};

export enum Gender{
  male='male',
  female='female',
  other='other'
}

export default TUser;