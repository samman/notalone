type TMeetup = {
  _id: string;
  channel_name: string;
  purpose: string;
  start_time: string;
  end_time: string;
  community_id: string;
  convener_id: string;
  cancelled: boolean;
};

export default TMeetup;
