import axios from "axios";
import env from "../environment";

const uploadProfilePic = (token: string, formData: object) => {
    return axios.patch(`${env.API_BASE_URL}/api/v1/users/updateProfilePic`,formData,{
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}

export {
    uploadProfilePic
}
