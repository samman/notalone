import axios from "axios";
import env from "../environment";

function login(data: object): object{
    return axios.post(`${env.API_BASE_URL}/api/v1/auth/login`, data);
}

function signup(data: object): object{
    return axios.post(`${env.API_BASE_URL}/api/v1/auth/signup`, data);
}

export {
    login,
    signup
}