import axios from 'axios';
import env from '../environment';
import AsyncStorage from '@react-native-async-storage/async-storage';

const createPost = async (data: object) => {
  const token = await AsyncStorage.getItem('jwt_token');
  return axios.post(`${env.API_BASE_URL}/api/v1/posts`, data, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

const uploadPostImage = async (id: string, formData: any) => {
  const token = await AsyncStorage.getItem('jwt_token');
  return axios.post(
    `${env.API_BASE_URL}/api/v1/posts/${id}/uploadPostImage`,
    formData,
    {
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'multipart/form-data',
      },
    },
  );
};

const getJoinedCommunityPosts = async (params: object) => {
  const token = await AsyncStorage.getItem('jwt_token');
  return axios.get(`${env.API_BASE_URL}/api/v1/posts/getJoinedCommunityPosts`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
    params,
  });
};

const likePost = async (postId: string) => {
  const token = await AsyncStorage.getItem('jwt_token');
  return axios.post(
    `${env.API_BASE_URL}/api/v1/posts/${postId}/like`,
    {},
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    },
  );
};

const unlikePost = async (postId: string) => {
  const token = await AsyncStorage.getItem('jwt_token');
  return axios.delete(`${env.API_BASE_URL}/api/v1/posts/${postId}/like`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

const deletePost = async (postId: string) => {
  const token = await AsyncStorage.getItem('jwt_token');
  return axios.delete(`${env.API_BASE_URL}/api/v1/posts/${postId}`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

const flagPostInappropriate = async (postId: string, reason: string) => {
  const token = await AsyncStorage.getItem('jwt_token');
  return axios.patch(
    `${env.API_BASE_URL}/api/v1/posts/${postId}/flag_inappropriate`,
    {reason},
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    },
  );
};

const removeFlag = async (postId: string, flagId: string) => {
  const token = await AsyncStorage.getItem('jwt_token');
  return axios.delete(
    `${env.API_BASE_URL}/api/v1/posts/${postId}/flag/${flagId}`,
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    },
  );
};

const voteInFavorOfFlag = async (postId: string, flagId: string) => {
  const token = await AsyncStorage.getItem('jwt_token');
  return axios.post(
    `${env.API_BASE_URL}/api/v1/posts/${postId}/flag/${flagId}/vote_in_favor`,
    {},
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    },
  );
};

const revokeVote = async (postId: string, flagId: string) => {
  const token = await AsyncStorage.getItem('jwt_token');
  return axios.delete(
    `${env.API_BASE_URL}/api/v1/posts/${postId}/flag/${flagId}/vote`,
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    },
  );
};

const postComment = async (postId: string, data: {content: string}) => {
  const token = await AsyncStorage.getItem('jwt_token');
  return axios.post(
    `${env.API_BASE_URL}/api/v1/posts/${postId}/comment`,
    data,
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    },
  );
};

export {
  createPost,
  uploadPostImage,
  getJoinedCommunityPosts,
  likePost,
  unlikePost,
  deletePost,
  flagPostInappropriate,
  removeFlag,
  voteInFavorOfFlag,
  revokeVote,
  postComment,
};
