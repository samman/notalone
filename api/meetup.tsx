import axios from 'axios';
import env from '../environment';
import AsyncStorage from '@react-native-async-storage/async-storage';

const getJoinedCommunityMeetups = async (params: object) => {
  const token = await AsyncStorage.getItem('jwt_token');
  return axios.get(`${env.API_BASE_URL}/api/v1/meetups`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
    params,
  });
};

const createMeetup = async (data: object) => {
  const token = await AsyncStorage.getItem('jwt_token');
  return axios.post(`${env.API_BASE_URL}/api/v1/meetups`, data, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

const updateMeetup = async (meetupId: string, data: object) => {
  const token = await AsyncStorage.getItem('jwt_token');
  return axios.patch(`${env.API_BASE_URL}/api/v1/meetups/${meetupId}`, data, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

export {getJoinedCommunityMeetups, createMeetup, updateMeetup};
