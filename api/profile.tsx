import axios from 'axios';
import env from '../environment';
import AsyncStorage from '@react-native-async-storage/async-storage';

const getProfile = async () => {
  const token = await AsyncStorage.getItem('jwt_token');
  return axios.get(`${env.API_BASE_URL}/api/v1/auth/profile`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

export {getProfile};
