import axios from 'axios';
import env from '../environment';
import AsyncStorage from '@react-native-async-storage/async-storage';

const getAllCommunities = async (
  params?: object,
) => {
  const token = await AsyncStorage.getItem('jwt_token');
  return axios.get(`${env.API_BASE_URL}/api/v1/communities`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
    params
  });
};

const getCommunity = async (communityId: string) => {
  const token = await AsyncStorage.getItem('jwt_token');
  return axios.get(`${env.API_BASE_URL}/api/v1/communities/${communityId}`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

const createCommunity = async (data: object) => {
  const token = await AsyncStorage.getItem('jwt_token');
  return axios.post(`${env.API_BASE_URL}/api/v1/communities`, data, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

const updateCommunity = async (data: object) => {
  const token = await AsyncStorage.getItem('jwt_token');
  return axios.patch(`${env.API_BASE_URL}/api/v1/communities`, data, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

const deleteCommunity = async (communityId: string) => {
  const token = await AsyncStorage.getItem('jwt_token');
  return axios.delete(`${env.API_BASE_URL}/api/v1/communities/${communityId}`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

const uploadCommunityLogo = async (communityId: string, formData: object) => {
  const token = await AsyncStorage.getItem('jwt_token');
  return axios.patch(
    `${env.API_BASE_URL}/api/v1/communities/${communityId}/updateCommunityLogo`,
    formData,
    {
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'multipart/form-data'
      },
    },
  );
};

const joinCommunity = async (communityId: string) => {
  const token = await AsyncStorage.getItem('jwt_token');
  return axios.post(
    `${env.API_BASE_URL}/api/v1/communities/${communityId}/join`,
    {},
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    },
  );
};

const leaveCommunity = async (communityId: string) => {
  const token = await AsyncStorage.getItem('jwt_token');
  return axios.post(
    `${env.API_BASE_URL}/api/v1/communities/${communityId}/leave`,
    {},
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    },
  );
};

export {
  getAllCommunities,
  getCommunity,
  createCommunity,
  updateCommunity,
  deleteCommunity,
  uploadCommunityLogo,
  joinCommunity,
  leaveCommunity,
};
