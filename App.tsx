/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import CommunitiesScreen from './screens/community/CommunitiesScreen';
import {StyleSheet, Text} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import PostsScreen from './screens/posts/PostsScreen';
import ProfileScreen from './screens/profile/ProfileScreen';
import MeetupsScreen from './screens/meetups/MeetupsScreen';

import {CommunitiesContextProvider} from './contexts/CommuntiesContext';
import {PostsContextProvider} from './contexts/PostsContext';
import {ParentDataContextProvider} from './contexts/ParentDataContext';
import {MeetupsContextProvider} from './contexts/MeetupsContext';

const Tab = createBottomTabNavigator();

const App = () => {

  return (
    <ParentDataContextProvider>
      <Tab.Navigator
        initialRouteName="Home"
        screenOptions={({route}) => ({
          tabBarIcon: ({color, size}) => {
            let iconName: string = '';

            if (route.name === 'Communities') {
              iconName = 'institution';
            } else if (route.name === 'Profile') {
              iconName = 'user';
            } else if (route.name === 'Home') {
              iconName = 'home';
            } else if (route.name === 'Meetup') {
              iconName = 'users';
            }

            // You can return any component that you like here!
            return <Icon name={iconName} size={size} color={color} />;
          },
        })}
        tabBarOptions={{
          keyboardHidesTabBar: true,
          activeTintColor: '#ee7269',
          inactiveTintColor: '#faf4e2',
          style: style.TabContainer,
        }}>
        <Tab.Screen
          name="Home"
          children={(props) => (
            <PostsContextProvider>
              <PostsScreen {...props} />
            </PostsContextProvider>
          )}
        />
        <Tab.Screen
          name="Communities"
          children={(props) => (
            <CommunitiesContextProvider>
              <CommunitiesScreen {...props} />
            </CommunitiesContextProvider>
          )}
        />
        <Tab.Screen name="Profile" component={ProfileScreen} />
        <Tab.Screen
          name="Meetup"
          children={(props) => (
            <MeetupsContextProvider>
              <MeetupsScreen {...props} />
            </MeetupsContextProvider>
          )}
        />
      </Tab.Navigator>
    </ParentDataContextProvider>
  );
};

const style = StyleSheet.create({
  TabContainer: {
    width: '90%',
    borderRadius: 10,
    marginHorizontal: '5%',
    marginVertical: 5,
    position: 'absolute',
    backgroundColor: '#2a2a2a',
    color: 'white',
  },
});

export default App;
