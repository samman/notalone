import {useEffect, useState} from 'react';
import {TLoggedInUser} from '../contexts/AuthContext';
import TPost from '../types/TPost';

export enum EWhatMustBeDisplayed {
  DELETE = 'delete',
  REVOKE_FLAG = 'revokeFlag',
  REVOKE_VOTE = 'remokeVote',
  VOTE = 'vote',
  FLAG = 'flag',
  REMOVE = 'REMOVE',
}

const usePostListBottomSheetOptions = (
  post: TPost | null,
  loggedInUser: TLoggedInUser | null,
) => {
  const [
    whatMustBeDisplayed,
    setWhatMustBeDisplayed,
  ] = useState<EWhatMustBeDisplayed>();

  useEffect(() => {
    if (post && loggedInUser) {
      if (post.author === loggedInUser?._id) {
        setWhatMustBeDisplayed(EWhatMustBeDisplayed.DELETE);
      } else if (post.flags.length) {
        if (post.community.leader === loggedInUser._id) {
          setWhatMustBeDisplayed(EWhatMustBeDisplayed.REMOVE);
        } else if (post.flags[0].flagged_by === loggedInUser._id) {
          setWhatMustBeDisplayed(EWhatMustBeDisplayed.REVOKE_FLAG);
        } else if (
          post.flags[0].votes.find((vote) => vote.vote_by === loggedInUser?._id)
        ) {
          setWhatMustBeDisplayed(EWhatMustBeDisplayed.REVOKE_VOTE);
        } else {
          setWhatMustBeDisplayed(EWhatMustBeDisplayed.VOTE);
        }
      } else {
        setWhatMustBeDisplayed(EWhatMustBeDisplayed.FLAG);
      }
    }
  }, [post]);

  return whatMustBeDisplayed;
};

export default usePostListBottomSheetOptions;
