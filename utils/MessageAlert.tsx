import { showMessage } from "react-native-flash-message";

export enum MessageAlertType {
  SUCCESS= "#5cb85c",
  ERROR= "#d95551"
}

const MessageAlert = (message: string, alertType: MessageAlertType) => {
  return showMessage({
    message,
    type: "default",
    backgroundColor: alertType, // background color
    color: "white", // text color
  });
};

export default MessageAlert;