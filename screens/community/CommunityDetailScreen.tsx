import React, {useState, useEffect} from 'react';
import {ActivityIndicator, StyleSheet, View} from 'react-native';
import {Button, ListItem, Text} from 'react-native-elements';
import {TLoggedInUser} from '../../contexts/AuthContext';

// Types
import TCommunity from '../../types/TCommunity';

type TCommunityDetailProps = {
  community: TCommunity;
  isCommunitiesLoading: boolean;
  handleJoin: (community: TCommunity) => void;
  handleLeave: (community: TCommunity) => void;
  handleDelete: (community: TCommunity) => void;
  isJoinLeaveLoading: boolean;
  isDeleteLoading: boolean;
  loggedInUser: TLoggedInUser;
};

const CommunityDetailScreen: React.FC<TCommunityDetailProps> = ({
  community,
  isCommunitiesLoading,
  handleJoin,
  handleLeave,
  isJoinLeaveLoading,
  isDeleteLoading,
  handleDelete,
  loggedInUser,
}) => {
  const [isCurrentUserLeader, setIsCurrentUserLeader] = useState<boolean>();

  useEffect(() => {
    if (community) {
      setIsCurrentUserLeader(loggedInUser?._id === community.leader._id);
    }
  }, [community]);

  return (
    <View>
      {isCommunitiesLoading && (
        <View style={styles.ActivityIndicatorView}>
          <ActivityIndicator size="large" color="#8086F2" />
        </View>
      )}
      {!isCommunitiesLoading && community && loggedInUser && (
        <View>
          <ListItem containerStyle={styles.ListItemContainer} bottomDivider>
            <ListItem.Content>
              <View style={styles.CommunityDetailItem}>
                <View style={styles.LabelContainer}>
                  <Text style={styles.Name}>Name</Text>
                </View>
                <View style={styles.TextContainer}>
                  <Text>{community.name}</Text>
                </View>
              </View>
            </ListItem.Content>
          </ListItem>
          <ListItem containerStyle={styles.ListItemContainer} bottomDivider>
            <ListItem.Content>
              <View style={styles.CommunityDetailItem}>
                <View style={styles.LabelContainer}>
                  <Text style={styles.Name}>Description</Text>
                </View>
                <View style={styles.TextContainer}>
                  <Text>{community.description}</Text>
                </View>
              </View>
            </ListItem.Content>
          </ListItem>
          <ListItem containerStyle={styles.ListItemContainer} bottomDivider>
            <ListItem.Content>
              <View style={styles.CommunityDetailItem}>
                <View style={styles.LabelContainer}>
                  <Text style={styles.Name}>Privacy</Text>
                </View>
                <View style={styles.TextContainer}>
                  <Text>{community.privacy}</Text>
                </View>
              </View>
            </ListItem.Content>
          </ListItem>
          <ListItem containerStyle={styles.ListItemContainer} bottomDivider>
            <ListItem.Content>
              <View style={styles.CommunityDetailItem}>
                <View style={styles.LabelContainer}>
                  <Text style={styles.Name}>Leader</Text>
                </View>
                <View style={styles.TextContainer}>
                  <Text>{community.leader.name}</Text>
                </View>
              </View>
            </ListItem.Content>
          </ListItem>
          <View style={styles.ButtonContainer}>
            {isCurrentUserLeader ? (
              <>
                <Button
                  title="View Members"
                  containerStyle={styles.ViewMembersButtonContainer}
                />
                <Button
                  title="Delete"
                  buttonStyle={styles.DeleteCommunityButtonContainer}
                  onPress={() => handleDelete(community)}
                  loading={isDeleteLoading}
                  disabled={isDeleteLoading}
                />
              </>
            ) : community.is_member ? (
              <Button
                title="Leave"
                containerStyle={styles.LeaveButtonContainer}
                buttonStyle={styles.LeaveButton}
                titleStyle={styles.LeaveButtonTitle}
                type="outline"
                loading={isJoinLeaveLoading}
                disabled={isJoinLeaveLoading}
                onPress={() => handleLeave(community)}
              />
            ) : (
              <Button
                title="Join"
                containerStyle={styles.JoinButtonContainer}
                onPress={() => handleJoin(community)}
                loading={isJoinLeaveLoading}
                disabled={isJoinLeaveLoading}
                disabledStyle={styles.DisabledButton}
              />
            )}
          </View>
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  CommunityDetailItem: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    height: 50,
  },
  ListItemContainer: {
    padding: 0,
  },
  LabelContainer: {
    backgroundColor: '#2288dd',
    width: '50%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  TextContainer: {
    width: '50%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  Name: {
    fontSize: 16,
    color: 'white',
    fontWeight: '600',
  },
  ButtonContainer: {
    justifyContent: 'flex-end',
    flexDirection: 'row',
    padding: 10,
  },
  JoinButtonContainer: {
    width: '20%',
  },
  LeaveButtonContainer: {
    width: '20%',
  },
  ViewMembersButtonContainer: {
    width: '40%',
    marginHorizontal: 4,
  },
  LeaveButton: {
    borderColor: 'red',
    color: '#d9534f',
  },
  LeaveButtonTitle: {
    color: '#d9534f',
  },
  ActivityIndicatorView: {padding: 100},
  DisabledButton: {backgroundColor: '#80b0d9'},
  DeleteCommunityButtonContainer: {
    backgroundColor: '#dc3545',
  },
});

export default CommunityDetailScreen;
