import React, {useState, useContext, useEffect, useRef} from 'react';
import {Alert, Platform, StyleSheet, Text, View} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import ImagePicker from 'react-native-image-crop-picker';
import {ErrorContext} from '../../contexts/ErrorContext';
import {CommunitiesContext} from '../../contexts/CommuntiesContext';
import {AuthContext} from '../../contexts';

import {
  createCommunity,
  uploadCommunityLogo,
  joinCommunity,
  leaveCommunity,
  deleteCommunity,
} from '../../api/community';
import MessageAlert, {MessageAlertType} from '../../utils/MessageAlert';

// Screens
import CommunitiesList from './CommunitiesList';
import CreateCommunityScreen from './CreateCommunityScreen';
import CommunityDetailScreen from './CommunityDetailScreen';

// Types
import TCommunity from '../../types/TCommunity';
import {ParentDataContext} from '../../contexts/ParentDataContext';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Button} from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import StackScreenHeader from '../../components/StackScreenHeader';

type TCommunityParams = {
  navigation: any;
};

const CommunityScreen = ({navigation}: TCommunityParams) => {
  const {
    communities,
    isCommunitiesLoading,
    setShouldLoadMore,
    setSearchTerm,
  } = useContext(CommunitiesContext);
  const [selectedCommunity, setSelectedCommunity] = useState<TCommunity>();
  const [
    shouldNavigateToCommunityDetail,
    setShouldNavigateToCommunityDetail,
  ] = useState<boolean>(false);
  const [
    shouldNavigateToCreatePost,
    setShouldNavigateToCreatePost,
  ] = useState<boolean>(false);
  const [
    shouldNavigateToCreateMeetup,
    setShouldNavigateToCreateMeetup,
  ] = useState<boolean>(false);
  const [isCreateLoading, setIsCreateLoading] = useState<boolean>(false);
  const [isJoinLeaveLoading, setIsJoinLeaveLoading] = useState<boolean>(false);
  const [isDeleteLoading, setIsDeleteLoading] = useState<boolean>(false);
  const [logo, setLogo] = useState<{path: string; mime: string}>();
  const {loggedInUser} = useContext(AuthContext);

  const {setShouldComunitiesDataUpdate, setShouldPostsDataUpdate} = useContext(
    ParentDataContext,
  );

  const {handleError} = useContext(ErrorContext);
  const Stack = createStackNavigator();

  useEffect(() => {
    setSelectedCommunity((prev) =>
      communities.find((community) => community._id === prev?._id),
    );
  }, [communities]);

  useEffect(() => {
    if (shouldNavigateToCommunityDetail) {
      navigation.navigate('Community Details');
      setShouldNavigateToCommunityDetail(false);
    }
  }, [shouldNavigateToCommunityDetail]);

  useEffect(() => {
    if (shouldNavigateToCreatePost) {
      navigation.navigate('Home', {
        screen: 'Create Post',
        params: {community: selectedCommunity},
      });
      setShouldNavigateToCreatePost(false);
    }
  }, [shouldNavigateToCreatePost]);

  useEffect(() => {
    if (shouldNavigateToCreateMeetup) {
      navigation.navigate('Meetup', {
        screen: 'Create Meetup',
        params: {community: selectedCommunity},
      });
      setShouldNavigateToCreateMeetup(false);
    }
  }, [shouldNavigateToCreateMeetup]);

  const selectLogoFormGallery: () => void = (): void => {
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: true,
    }).then((image) => {
      setLogo(image);
    });
  };

  const captureLogo: () => void = (): void => {
    ImagePicker.openCamera({
      width: 300,
      height: 400,
      cropping: true,
    })
      .then((image) => {
        setLogo(image);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleSubmit = async (data: any, actions: any): Promise<any> => {
    try {
      setIsCreateLoading(true);
      const res = await createCommunity(data);
      if (logo) {
        let formData = new FormData();
        formData.append('community_logo', {
          name: logo.path.split('/').pop(),
          type: logo.mime,
          uri:
            Platform.OS === 'android'
              ? logo.path
              : logo.path.replace('file://', ''),
        });
        await uploadCommunityLogo(res.data.community._id, formData);
      }
      setIsCreateLoading(false);
      setShouldComunitiesDataUpdate(true);
      MessageAlert('Community successfully created', MessageAlertType.SUCCESS);
      actions.resetForm();
      setLogo(undefined);
      navigation.navigate('Community List');
    } catch (err) {
      setIsCreateLoading(false);
      handleError(err);
    }
  };

  const handleViewCommunityDetails = (community: TCommunity) => {
    setSelectedCommunity(community);
    setShouldNavigateToCommunityDetail(true);
  };

  const handleCreatePost = (community: TCommunity) => {
    setSelectedCommunity(community);
    setShouldNavigateToCreatePost(true);
  };

  const handleOrganizeMeetup = (community: TCommunity) => {
    setSelectedCommunity(community);
    setShouldNavigateToCreateMeetup(true);
  };

  const handleJoin = async (community: TCommunity) => {
    try {
      setIsJoinLeaveLoading(true);
      await joinCommunity(community._id);
      setShouldComunitiesDataUpdate(true);
      setShouldPostsDataUpdate(true);
      MessageAlert(
        `${community.name} successfully joined`,
        MessageAlertType.SUCCESS,
      );
    } catch (err) {
      handleError(err);
    } finally {
      setIsJoinLeaveLoading(false);
    }
  };

  const handleLeave = async (community: TCommunity) => {
    try {
      setIsJoinLeaveLoading(true);
      await leaveCommunity(community._id);
      setShouldComunitiesDataUpdate(true);
      setShouldPostsDataUpdate(true);
      MessageAlert(
        `${community.name} successfully left`,
        MessageAlertType.SUCCESS,
      );
    } catch (err) {
      handleError(err);
    } finally {
      setIsJoinLeaveLoading(false);
    }
  };

  const handleDelete = async (community: TCommunity) => {
    try {
      setIsDeleteLoading(true);
      await deleteCommunity(community._id);
      setShouldComunitiesDataUpdate(true);
      setShouldPostsDataUpdate(true);
      MessageAlert(
        `${community.name} successfully deleted`,
        MessageAlertType.SUCCESS,
      );
    } catch (err) {
      handleError(err);
    } finally {
      setIsDeleteLoading(false);
    }
  };

  const handleScroll = ({nativeEvent}: any) => {
    const bottom =
      nativeEvent.layoutMeasurement.height + nativeEvent.contentOffset.y >=
      nativeEvent.contentSize.height - 5;
    if (bottom) {
      setShouldLoadMore(true);
    }
  };

  const redirectToCreateCommunity = () => {
    navigation.navigate('Create Community');
  };

  const leaveConfirmation = (community: TCommunity) => {
    Alert.alert(
      'Leave Confirmation',
      `Are you sure you want to leave ${community.name}`,
      [
        {
          text: 'Cancel',
          style: 'cancel',
        },
        {text: 'Yes, I am sure', onPress: () => handleLeave(community)},
      ],
      {cancelable: true},
    );
  };

  const handleSearchTermChange = (term: string) => {
    setSearchTerm(term);
  };

  return (
    <>
      <Stack.Navigator initialRouteName="Community List">
        {loggedInUser && (
          <>
            <Stack.Screen
              name="Community List"
              options={{
                header: (props) => (
                  <StackScreenHeader
                    text="Communities"
                    innerComponent={() => (
                      <LinearGradient
                        colors={['#f0c666', '#f79b79']}
                        start={{x: 0, y: 0}}
                        end={{x: 1, y: 0}}
                        style={styles.LinearGradient}>
                        <Button
                          icon={
                            <Icon
                              name="plus"
                              size={10}
                              style={styles.ButtonIcon}
                              color="white"
                            />
                          }
                          buttonStyle={styles.Button}
                          onPress={redirectToCreateCommunity}
                        />
                      </LinearGradient>
                    )}
                    {...props}
                  />
                ),
              }}>
              {(props) => (
                <CommunitiesList
                  communities={communities}
                  isCommunitiesLoading={isCommunitiesLoading}
                  onViewCommunityDetails={handleViewCommunityDetails}
                  onCreatePost={handleCreatePost}
                  handleScroll={handleScroll}
                  handleRefresh={() => setShouldComunitiesDataUpdate(true)}
                  loggedInUser={loggedInUser}
                  onOrganizeMeetup={handleOrganizeMeetup}
                  onSearchTermChange={handleSearchTermChange}
                  {...props}
                />
              )}
            </Stack.Screen>
            <Stack.Screen
              name="Create Community"
              options={{
                header: (props) => (
                  <StackScreenHeader
                    text="Create Community"
                    enableReturn={true}
                    {...props}
                  />
                ),
              }}>
              {(props) => (
                <CreateCommunityScreen
                  handleSubmit={handleSubmit}
                  isCreateLoading={isCreateLoading}
                  captureLogo={captureLogo}
                  selectLogoFormGallery={selectLogoFormGallery}
                  logo={logo}
                  {...props}
                />
              )}
            </Stack.Screen>

            {selectedCommunity && (
              <Stack.Screen
                name="Community Details"
                options={{
                  headerStyle: {
                    backgroundColor: '#6960f7',
                  },
                  headerTintColor: '#fff',
                  headerTitleStyle: {
                    fontWeight: 'bold',
                  },
                }}>
                {(props) => (
                  <CommunityDetailScreen
                    {...props}
                    isCommunitiesLoading={isCommunitiesLoading}
                    community={selectedCommunity}
                    handleJoin={handleJoin}
                    isJoinLeaveLoading={isJoinLeaveLoading}
                    isDeleteLoading={isDeleteLoading}
                    handleLeave={leaveConfirmation}
                    handleDelete={handleDelete}
                    loggedInUser={loggedInUser}
                  />
                )}
              </Stack.Screen>
            )}
          </>
        )}
      </Stack.Navigator>
    </>
  );
};

const styles = StyleSheet.create({
  LinearGradient: {
    borderRadius: 100,
    width: 40,
    height: 40,
    paddingHorizontal: 0,
  },
  Button: {
    backgroundColor: 'transparent',
    borderRadius: 100,
    width: 40,
    height: 40,
  },
  ButtonIcon: {
    fontSize: 16,
    padding: 5,
  },
});

export default CommunityScreen;
