import React, {useState} from 'react';
import {
  StyleSheet,
  View,
  KeyboardAvoidingView,
  TouchableOpacity,
  Image,
  ScrollView,
} from 'react-native';
import {
  Input,
  Button,
  Card,
  Icon,
  BottomSheet,
  ListItem,
} from 'react-native-elements';
import {Formik} from 'formik';
import * as Yup from 'yup';
import {Picker} from '@react-native-picker/picker';

type TCommunityForm = {
  name: string;
  description: string;
  privacy: string;
};

type TCreateCommunityParams = {
  handleSubmit: (data: any, actions: any) => Promise<any>;
  isCreateLoading: boolean;
  captureLogo: () => void;
  selectLogoFormGallery: () => void;
  logo: {path: string} | undefined;
};

const CreateCommunitySchema = Yup.object().shape({
  name: Yup.string().required('Required'),
  description: Yup.string(),
  privacy: Yup.string().required('Required'),
});

function CreateCommunityScreen({
  handleSubmit,
  isCreateLoading,
  logo,
  selectLogoFormGallery,
  captureLogo,
}: TCreateCommunityParams) {
  const [isBottomSheetVisible, setIsBottomSheetVisible] = useState(false);
  const initialValues: TCommunityForm = {
    name: '',
    description: '',
    privacy: 'public',
  };

  return (
    <>
      <ScrollView>
        <View style={styles.CommunityFormContainer}>
          <BottomSheet
            isVisible={isBottomSheetVisible}
            modalProps={{}}
            containerStyle={{
              backgroundColor: 'rgba(0.5, 0.25, 0, 0.2)',
            }}>
            <ListItem
              onPress={() => {
                setIsBottomSheetVisible(false);
                selectLogoFormGallery();
              }}>
              <ListItem.Content>
                <ListItem.Title>Open galery</ListItem.Title>
              </ListItem.Content>
            </ListItem>
            <ListItem
              onPress={() => {
                setIsBottomSheetVisible(false);
                captureLogo();
              }}>
              <ListItem.Content>
                <ListItem.Title>Open camera</ListItem.Title>
              </ListItem.Content>
            </ListItem>
            <ListItem
              containerStyle={{backgroundColor: 'grey'}}
              onPress={() => setIsBottomSheetVisible(false)}>
              <ListItem.Content>
                <ListItem.Title style={{color: 'white'}}>Cancel</ListItem.Title>
              </ListItem.Content>
            </ListItem>
          </BottomSheet>
          <Formik
            initialValues={initialValues}
            validationSchema={CreateCommunitySchema}
            onSubmit={handleSubmit}>
            {({
              handleChange,
              handleBlur,
              handleSubmit,
              values,
              errors,
              touched,
              setFieldValue,
            }) => (
              <KeyboardAvoidingView behavior="position">
                <View style={styles.LogoSelectorContainer}>
                  <TouchableOpacity
                    onPress={() => setIsBottomSheetVisible(true)}
                    style={styles.LogoSelector}>
                    {logo ? (
                      <>
                        <Image source={{uri: logo.path}} style={styles.logo} />
                      </>
                    ) : (
                      <Icon name="image" size={50} color="white" />
                    )}
                  </TouchableOpacity>
                  <Icon name="camera" color="red" style={styles.CameraIcon} />
                </View>
                <Card containerStyle={styles.CommunityFormForm}>
                  <View style={styles.InputView}>
                    <Input
                      label="Community Name"
                      placeholder="Enter community name"
                      onChangeText={handleChange('name')}
                      onBlur={handleBlur('name')}
                      value={values.name}
                      errorMessage={
                        errors.name && touched.name ? errors.name : ''
                      }
                    />
                  </View>
                  <View style={styles.InputView}>
                    <Input
                      label="Description"
                      placeholder="Tell something about the group"
                      onChangeText={handleChange('description')}
                      onBlur={handleBlur('description')}
                      value={values.description}
                      errorMessage={
                        errors.description && touched.description
                          ? errors.description
                          : ''
                      }
                    />
                  </View>
                  <View style={styles.InputView}>
                    <Input
                      label="Privacy"
                      placeholder="Enter privacy constraint"
                      onBlur={handleBlur('privacy')}
                      errorMessage={
                        errors.privacy && touched.privacy ? errors.privacy : ''
                      }
                      disabled={true}
                      InputComponent={() => (
                        <Picker
                          selectedValue={values.privacy}
                          style={styles.PrivacyPicker}
                          onValueChange={(itemValue) => {
                            setFieldValue('privacy', itemValue);
                          }}>
                          <Picker.Item label="Public" value="public" />
                          <Picker.Item label="Private" value="private" />
                        </Picker>
                      )}
                    />
                  </View>
                  <View style={styles.CreateCommunityScreenBtnContainer}>
                    <Button
                      title="Save"
                      onPress={handleSubmit}
                      loading={isCreateLoading}
                      disabled={isCreateLoading}
                      disabledStyle={styles.DisabledButton}
                    />
                  </View>
                </Card>
              </KeyboardAvoidingView>
            )}
          </Formik>
        </View>
      </ScrollView>
    </>
  );
}

const styles = StyleSheet.create({
  CommunityFormContainer: {
    width: '80%',
    marginLeft: '10%',
    marginTop: '20%',
  },

  image: {
    height: 150,
    width: 150,
  },

  InputView: {
    width: '100%',
    height: 40,
    marginBottom: 50,
    alignItems: 'center',
  },

  CommunityFormForm: {
    borderRadius: 10,
    marginHorizontal: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },

  CreateCommunityScreenBtnContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    width: '100%',
  },

  CreateCommunityScreenBtn: {
    width: 60,
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
    backgroundColor: '#ff6666',
    marginLeft: 10,
  },

  CreateCommunityScreenSigninContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 10,
  },

  CommunityFormText: {
    color: '#5eaaa8',
    textDecorationStyle: 'dashed',
  },

  InputIcon: {
    color: '#86939e',
  },

  logoView: {
    margin: 10,
  },

  CameraIcon: {
    position: 'absolute',
    bottom: '0%',
    right: '0%',
    fontSize: 16,
  },

  LogoSelector: {
    height: 100,
    width: 100,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#5253d9',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
    overflow: 'hidden',
  },

  logo: {
    height: '100%',
    width: '100%',
  },

  DisabledButton: {
    backgroundColor: '#94bee3',
  },

  LogoSelectorContainer: {
    alignItems: 'center',
  },

  PrivacyPicker: {height: 50, width: '100%'},
});

export default CreateCommunityScreen;
