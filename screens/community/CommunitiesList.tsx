import React, {useEffect, useState} from 'react';
import {StyleSheet, View, Text, RefreshControl} from 'react-native';
import {
  ListItem,
  Avatar,
  Button,
  BottomSheet,
  Badge,
  SearchBar,
} from 'react-native-elements';
import {ScrollView} from 'react-native-gesture-handler';
import LinearGradient from 'react-native-linear-gradient';

import {TLoggedInUser} from '../../contexts/AuthContext';

// Type
import TCommunity from '../../types/TCommunity';

import env from '../../environment';
import {colorGradients} from '../../GlobalStyles';
import {Dimensions} from 'react-native';

type TCommunitiesParams = {
  navigation: any;
  communities: Array<TCommunity>;
  isCommunitiesLoading: boolean;
  handleRefresh: () => void;
  onViewCommunityDetails: (community: TCommunity) => void;
  onCreatePost: (community: TCommunity) => void;
  onOrganizeMeetup: (community: TCommunity) => void;
  onSearchTermChange: (searchTerm: string) => void;
  handleScroll: (e: any) => void;
  loggedInUser: TLoggedInUser;
};

const CommunitiesList = ({
  communities,
  isCommunitiesLoading,
  onViewCommunityDetails,
  onCreatePost,
  handleScroll,
  handleRefresh,
  onOrganizeMeetup,
  loggedInUser,
  onSearchTermChange,
}: TCommunitiesParams) => {
  const [pressedCommunity, setPressedCommunity] = useState<TCommunity>();
  const [isBottomSheetVisible, setIsBottomSheetVisible] = useState<boolean>(
    false,
  );
  const [isCommunityLeader, setIsCommunityLeader] = useState<boolean>(false);
  const [searchTerm, setSearchTerm] = useState<string>('');

  useEffect(() => {
    onSearchTermChange(searchTerm)
  }, [searchTerm])

  useEffect(() => {
    if (pressedCommunity?.leader._id === loggedInUser?._id)
      setIsCommunityLeader(true);
    else setIsCommunityLeader(false);
  }, [pressedCommunity]);

  const handleCommunityPress = (community: TCommunity) => {
    setPressedCommunity(community);
    setIsBottomSheetVisible(true);
  };

  const handleCreatePostPress = () => {
    if (pressedCommunity) {
      onCreatePost(pressedCommunity);
    }
    setIsBottomSheetVisible(false);
  };

  const handleOrganizeMeetupPress = () => {
    if (pressedCommunity) {
      onOrganizeMeetup(pressedCommunity);
    }
    setIsBottomSheetVisible(false);
  };

  const handleViewCommunityDetailsPress = () => {
    if (pressedCommunity) {
      onViewCommunityDetails(pressedCommunity);
    }
    setIsBottomSheetVisible(false);
  };

  const handleBottomSheetCancelPress = () => {
    setIsBottomSheetVisible(false);
  };

  return (
    <>
      <BottomSheet
        isVisible={isBottomSheetVisible}
        modalProps={{}}
        containerStyle={{
          backgroundColor: 'rgba(0.5, 0.25, 0, 0.2)',
        }}>
        {pressedCommunity?.is_member && (
          <ListItem onPress={handleCreatePostPress}>
            <ListItem.Content>
              <ListItem.Title>Create Post</ListItem.Title>
            </ListItem.Content>
          </ListItem>
        )}
        {isCommunityLeader && (
          <ListItem onPress={handleOrganizeMeetupPress}>
            <ListItem.Content>
              <ListItem.Title>Organize Community Meetup</ListItem.Title>
            </ListItem.Content>
          </ListItem>
        )}
        <ListItem
          onPress={() => {
            handleViewCommunityDetailsPress();
          }}>
          <ListItem.Content>
            <ListItem.Title>View Details</ListItem.Title>
          </ListItem.Content>
        </ListItem>
        <ListItem
          onPress={handleBottomSheetCancelPress}
          containerStyle={{backgroundColor: 'grey'}}>
          <ListItem.Content>
            <ListItem.Title style={{color: 'white'}}>Cancel</ListItem.Title>
          </ListItem.Content>
        </ListItem>
      </BottomSheet>
      <ScrollView
        onScroll={handleScroll}
        refreshControl={
          <RefreshControl
            refreshing={isCommunitiesLoading}
            onRefresh={handleRefresh}
          />
        }>
        <View style={styles.CommunitiesScreenContentContainer}>
          <SearchBar
            placeholder="Type Here..."
            value={searchTerm}
            lightTheme={true}
            containerStyle={{
              backgroundColor: '#ffffff',
            }}
            onChangeText={(value) => setSearchTerm(value)}
          />
          {!!communities.length && (
            <View>
              {communities.map((community, i) => (
                <View key={community._id}>
                  <LinearGradient
                    colors={colorGradients[i % colorGradients.length]}
                    start={{x: 0, y: 0}}
                    end={{x: 1, y: 0}}
                    style={styles.LinearGradient}>
                    <ListItem
                      key={i}
                      bottomDivider
                      containerStyle={styles.CommunityListItem}
                      underlayColor="none"
                      onPress={() => handleCommunityPress(community)}>
                      {community.community_logo && (
                        <Avatar
                          size={48}
                          source={{
                            uri: `${env.API_BASE_URL}/${community.community_logo}`,
                          }}
                        />
                      )}
                      <ListItem.Content>
                        <ListItem.Title>{community.name}</ListItem.Title>
                        <ListItem.Subtitle>
                          {community.description}
                        </ListItem.Subtitle>
                      </ListItem.Content>
                      {community.is_member && (
                        <Badge value="member" status="success" />
                      )}
                      <ListItem.Chevron />
                    </ListItem>
                  </LinearGradient>
                </View>
              ))}
            </View>
          )}
          {!communities.length && !isCommunitiesLoading && (
            <View style={styles.EmptyMessageContainer}>
              <Text style={styles.EmptyMessage}>No Results</Text>
            </View>
          )}
        </View>
      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  CommunityListItem: {
    backgroundColor: 'transparent',
  },
  ActivityIndicatorView: {marginTop: 10},
  EmptyMessageContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: 64,
    padding: 40,
  },
  EmptyMessage: {
    fontSize: 32,
  },
  CommunitiesScreenContentContainer: {
    marginBottom: '30%',
  },
  LinearGradient: {
    flex: 1,
    borderRadius: 10,
    marginHorizontal: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
    width: '95%',
    alignSelf: 'center',
    marginVertical: 5,
  },
});

export default CommunitiesList;
