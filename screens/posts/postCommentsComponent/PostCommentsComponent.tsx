import {Formik} from 'formik';
import React from 'react';
import * as Yup from 'yup';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  KeyboardAvoidingView,
} from 'react-native';
import {Avatar, Card, Input, ListItem, Button} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import TPost from '../../../types/TPost';

import env from '../../../environment';

type TPostCommentsComponentParams = {
  post: TPost;
  onClose: () => void;
  onSubmit: (data: {content: string}, action: any) => void;
  isSavingPostCommentLoading: boolean;
};

const PostCommentsComponent = ({
  post,
  onClose,
  onSubmit,
  isSavingPostCommentLoading,
}: TPostCommentsComponentParams) => {
  return (
    <Card containerStyle={styles.CommentsCard}>
      <View style={styles.ReturnButtonContainer}>
        <TouchableOpacity onPress={onClose}>
          <Icon name="arrow-left" size={20} color="#000000" />
        </TouchableOpacity>
      </View>
      <Card.Title>Comments</Card.Title>
      <View style={{height: '95%'}}>
        <View style={{flex: 1, overflow: 'hidden'}}>
          <ScrollView>
            {post.comments.map((c, i) => (
              <ListItem key={i} bottomDivider>
                <Avatar
                  source={{uri: `${env.API_BASE_URL}/${c.author?.profilePic}`}}
                  avatarStyle={{borderRadius: 100}}
                />
                <ListItem.Content
                  style={styles.CommentcontentAndContentContainer}>
                  <Text style={styles.CommentAuthor}>{c.author.username}</Text>
                  <Text style={styles.CommentContent}>{c.content}</Text>
                </ListItem.Content>
              </ListItem>
            ))}
          </ScrollView>
        </View>
        <KeyboardAvoidingView>
          <View style={styles.CommentInputAndPostButtonContainer}>
            <Formik
              initialValues={{content: ''}}
              validationSchema={Yup.object().shape({
                content: Yup.string().required('Required'),
              })}
              onSubmit={onSubmit}>
              {({
                handleChange,
                handleBlur,
                handleSubmit,
                values,
                errors,
                touched,
              }) => (
                <>
                  <Input
                    placeholder="Enter your comment"
                    onChangeText={handleChange('content')}
                    onBlur={handleBlur('content')}
                    value={values.content}
                    errorMessage={
                      errors.content && touched.content ? errors.content : ''
                    }
                  />
                  <Button
                    loading={isSavingPostCommentLoading}
                    disabled={isSavingPostCommentLoading}
                    title="post"
                    type="solid"
                    containerStyle={styles.PostCommentButton}
                    onPress={handleSubmit}
                  />
                </>
              )}
            </Formik>
          </View>
        </KeyboardAvoidingView>
      </View>
    </Card>
  );
};

const styles = StyleSheet.create({
  CommentsCard: {padding: 5, margin: 0, flex: 1},
  CommentcontentAndContentContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  CommentAuthor: {
    marginRight: 5,
    lineHeight: 18,
    fontSize: 18,
    fontWeight: 'bold',
  },
  CommentContent: {lineHeight: 16},
  CommentInputAndPostButtonContainer: {
    alignItems: 'flex-end',
    borderTopWidth: 0.5,
    borderTopColor: '#9e9e9e',
    borderColor: 'grey',
  },
  PostCommentButton: {width: 80, marginBottom: 10, marginRight: 10},
  EmptyMessageContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: 64,
    padding: 40,
  },
  EmptyMessage: {
    fontSize: 32,
    color: '#797f86',
  },
  ReturnButtonContainer: {position: 'absolute', zIndex: 1},
});

export default PostCommentsComponent;
