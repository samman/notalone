import React, {useContext, useRef, useState} from 'react';
import {
  StyleSheet,
  View,
  KeyboardAvoidingView,
  TouchableOpacity,
  Image,
  TextInput,
  Text,
  Platform,
} from 'react-native';
import {
  Input,
  Button,
  Card,
  Icon,
  BottomSheet,
  ListItem,
} from 'react-native-elements';
import {Formik} from 'formik';
import * as Yup from 'yup';
import ImagePicker from 'react-native-image-crop-picker';
import {ScrollView} from 'react-native-gesture-handler';
import MessageAlert, {MessageAlertType} from '../../utils/MessageAlert';
import {ErrorContext} from '../../contexts';
import {createPost, uploadPostImage} from '../../api/post';
import {ParentDataContext} from '../../contexts/ParentDataContext';

type TPostForm = {
  community: string;
  title: string;
  content: string;
  author: string;
  post_image: string | undefined;
  users_that_liked: Array<string>;
};

type TCreatePostParams = {
  route: any;
  navigation: any;
};

const CreatePostSchema = Yup.object().shape({
  title: Yup.string().required('Required'),
  content: Yup.string(),
});

function PostForm({route, navigation}: TCreatePostParams) {
  const [isCreateLoading, setIsCreateLoading] = useState(false);
  const [isBottomSheetVisible, setIsBottomSheetVisible] = useState(false);
  const [contentHeight, setContentHeight] = useState<number>(40);
  const initialValues: TPostForm = {
    community: route.params.community._id,
    title: '',
    content: '',
    author: '',
    post_image: '',
    users_that_liked: [],
  };
  const [postImage, setPostImage] = useState<{path: string; mime: string}>();
  const {handleError} = useContext(ErrorContext);
  const {setShouldPostsDataUpdate} = useContext(ParentDataContext);

  const selectpostImageFromGallery = () => {
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: true,
      mediaType: 'photo',
    }).then((image: any) => {
      setPostImage(image);
    });
  };

  const capturepostImage = () => {
    ImagePicker.openCamera({
      width: 300,
      height: 400,
      cropping: true,
      mediaType: 'photo',
    })
      .then((image: any) => {
        setPostImage(image);
      })
      .catch((err: any) => {
        console.log(err);
      });
  };

  const handleSubmit = async (data: any, actions: any) => {
    try {
      setIsCreateLoading(true);
      const res = await createPost(data);
      if (postImage) {
        let formData = new FormData();
        formData.append('post_image', {
          name: postImage.path.split('/').pop(),
          type: postImage.mime,
          uri:
            Platform.OS === 'android'
              ? postImage.path
              : postImage.path.replace('file://', ''),
        });
        await uploadPostImage(res.data.post._id, formData);
      }
      MessageAlert(`Post successfully created`, MessageAlertType.SUCCESS);
      actions.resetForm();
      setPostImage(undefined);
      setShouldPostsDataUpdate(true);
      navigation.navigate('Post List');
    } catch (err) {
      handleError(err);
    } finally {
      setIsCreateLoading(false);
    }
  };

  return (
    <>
      <ScrollView>
        <View style={styles.PostFormContainer}>
          <BottomSheet
            isVisible={isBottomSheetVisible}
            modalProps={{}}
            containerStyle={{
              backgroundColor: 'rgba(0.5, 0.25, 0, 0.2)',
            }}>
            <ListItem
              onPress={() => {
                setIsBottomSheetVisible(false);
                selectpostImageFromGallery();
              }}>
              <ListItem.Content>
                <ListItem.Title>Open galery</ListItem.Title>
              </ListItem.Content>
            </ListItem>
            <ListItem
              onPress={() => {
                setIsBottomSheetVisible(false);
                capturepostImage();
              }}>
              <ListItem.Content>
                <ListItem.Title>Open camera</ListItem.Title>
              </ListItem.Content>
            </ListItem>
            <ListItem
              containerStyle={{backgroundColor: 'red'}}
              onPress={() => setIsBottomSheetVisible(false)}>
              <ListItem.Content>
                <ListItem.Title style={{color: 'white'}}>Cancel</ListItem.Title>
              </ListItem.Content>
            </ListItem>
          </BottomSheet>
          <Formik
            initialValues={initialValues}
            validationSchema={CreatePostSchema}
            onSubmit={handleSubmit}>
            {({
              handleChange,
              handleBlur,
              handleSubmit,
              values,
              errors,
              touched,
            }) => (
              <View style={styles.PostForm}>
                <KeyboardAvoidingView behavior="position">
                  <View style={styles.postImageSelectorContainer}>
                    <TouchableOpacity
                      onPress={() => setIsBottomSheetVisible(true)}
                      style={styles.postImageSelector}>
                      {postImage ? (
                        <>
                          <Image
                            source={{uri: postImage.path}}
                            style={styles.postImage}
                          />
                        </>
                      ) : (
                        <Icon name="image" size={50} color="white" />
                      )}
                    </TouchableOpacity>
                    <Icon name="camera" color="red" style={styles.CameraIcon} />
                  </View>
                  <Card containerStyle={styles.PostFormForm}>
                    <View style={styles.InputView}>
                      <Input
                        label={'Title'}
                        placeholder="Enter post title"
                        onChangeText={handleChange('title')}
                        onBlur={handleBlur('title')}
                        value={values.title}
                        errorMessage={
                          errors.title && touched.title ? errors.title : ''
                        }
                      />
                    </View>
                    <View style={styles.InputView}>
                      <Text style={styles.InputLabel}>Content</Text>
                      <TextInput
                        style={[
                          styles.ContentTextInput,
                          {height: contentHeight},
                        ]}
                        placeholderTextColor="#86939e"
                        onChangeText={handleChange('content')}
                        onBlur={handleBlur('content')}
                        placeholder="Enter post content"
                        onContentSizeChange={(e) =>
                          setContentHeight(e.nativeEvent.contentSize.height)
                        }
                        editable={true}
                        multiline={true}
                      />
                    </View>
                    <View style={styles.CreatePostBtnContainer}>
                      <Button
                        title="Save"
                        onPress={handleSubmit}
                        loading={isCreateLoading}
                        disabled={isCreateLoading}
                        disabledStyle={styles.DisabledButton}
                      />
                    </View>
                  </Card>
                </KeyboardAvoidingView>
              </View>
            )}
          </Formik>
        </View>
      </ScrollView>
    </>
  );
}

const styles = StyleSheet.create({
  PostFormContainer: {
    width: '100%',
    marginVertical: 20,
    alignItems: 'center',
  },
  PostForm: {width: '90%'},

  InputView: {
    width: '100%',
  },

  PostFormForm: {
    borderRadius: 10,
    marginHorizontal: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },

  PostFormBtnContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    width: '100%',
  },

  PostFormBtn: {
    width: 60,
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
    backgroundColor: '#ff6666',
    marginLeft: 10,
  },

  PostFormSigninContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 10,
  },

  PostFormText: {
    color: '#5eaaa8',
    textDecorationStyle: 'dashed',
  },

  InputIcon: {
    color: '#86939e',
  },

  postImageView: {
    margin: 10,
  },

  CameraIcon: {
    position: 'absolute',
    bottom: '0%',
    right: '0%',
    fontSize: 16,
  },

  postImageSelector: {
    height: 150,
    borderRadius: 10,
    width: 150,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#5253d9',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
    overflow: 'hidden',
  },

  postImage: {
    height: '100%',
    width: '100%',
  },

  DisabledButton: {
    backgroundColor: '#94bee3',
  },

  postImageSelectorContainer: {
    alignItems: 'center',
  },

  PrivacyPicker: {height: 50, width: '100%'},

  InputLabel: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#87949f',
    marginHorizontal: 10,
  },
  ContentTextInput: {
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderLeftWidth: 0,
    borderBottomWidth: 1,
    borderColor: '#87949f',
    borderWidth: 1,
    marginHorizontal: 10,
    fontSize: 18,
  },
  CreatePostBtnContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    width: '100%',
    marginTop: 20,
  },
});

export default PostForm;
