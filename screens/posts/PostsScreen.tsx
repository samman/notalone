import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import PostForm from './PostForm';
import PostList from './PostList';
import StackScreenHeader from '../../components/StackScreenHeader';

const PostsScreen = (props: any) => {
  const Stack = createStackNavigator();

  return (
    <>
      <Stack.Navigator initialRouteName="Post List">
        <Stack.Screen
          name="Post List"
          options={{
            header: (props) => (
              <StackScreenHeader
                text="Posts"
                {...props}
              />
            ),
          }}>
          {(props) => <PostList {...props} />}
        </Stack.Screen>
        <Stack.Screen
          name="Create Post"
          options={{
            header: (props) => (
              <StackScreenHeader
                text="Create Post"
                enableReturn={true}
                {...props}
              />
            ),
          }}>
          {(props) => <PostForm {...props} />}
        </Stack.Screen>
      </Stack.Navigator>
    </>
  );
};

export default PostsScreen;
