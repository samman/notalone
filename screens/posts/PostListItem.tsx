import React, {useEffect, useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {Badge, Card, Image, Text} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome5';
import TPost, {EPostFlag} from '../../types/TPost';

import env from '../../environment';
import {TouchableOpacity} from 'react-native-gesture-handler';
import LinearGradient from 'react-native-linear-gradient';
import {colorGradients} from '../../GlobalStyles';

type TPostListItemParams = {
  post: TPost;
  onEllipsisClick: (post: TPost) => void;
  loggedInUserId: string;
  onLikeClick: (post: TPost) => void;
  onUnlikeClick: (post: TPost) => void;
  onCommentClick: (post: TPost) => void;
  handleFlagBadgePress: (flag: {reason: string}) => void;
  index: number;
};

function PostListItem({
  post,
  onEllipsisClick,
  loggedInUserId,
  onLikeClick,
  onUnlikeClick,
  onCommentClick,
  handleFlagBadgePress,
  index,
}: TPostListItemParams) {
  const [hasLiked, setHasLiked] = useState<boolean>(false);

  useEffect(() => {
    setHasLiked(post.users_that_liked.includes(loggedInUserId));
  }, [post]);

  return (
    <LinearGradient
      colors={colorGradients[index % colorGradients.length]}
      start={{x: 0, y: 0}}
      end={{x: 1, y: 0}}
      style={styles.LinearGradient}>
      <Card containerStyle={styles.PostCard}>
        <View style={styles.CardTopItemsContainer}>
          {!!post.flags.length &&
            post.flags[0].flag === EPostFlag.inappropriate && (
              <Badge
                value={() => (
                  <View style={styles.FlagContainer}>
                    <Icon name={'flag'} size={16} color="white" solid={true} />
                  </View>
                )}
                status="warning"
                badgeStyle={{padding: 10, marginHorizontal: 10, height: 30}}
                onPress={() => handleFlagBadgePress(post.flags[0])}
              />
            )}
          <View style={styles.EllipsisContainer}>
            <TouchableOpacity
              onPress={() => {
                onEllipsisClick(post);
              }}
              style={styles.EllipsisTouchableOpacity}>
              <Icon name="ellipsis-h" size={20} color="black" solid={false} />
            </TouchableOpacity>
          </View>
        </View>
        <Card.Title style={styles.PostTitle}>{post.title}</Card.Title>
        <View style={styles.PostContentContainer}>
          <Text style={styles.PostContent}>{post.content}</Text>
          {!!post.post_image && (
            <Image
              style={styles.PostImage}
              resizeMode="cover"
              source={{uri: `${env.API_BASE_URL}/${post.post_image}`}}
            />
          )}
        </View>
        <View style={styles.PostLikeAndCommentContainer}>
          <TouchableOpacity
            onPress={() => {
              if (hasLiked) {
                onUnlikeClick(post);
              } else {
                onLikeClick(post);
              }
            }}
            style={styles.Like}>
            <Icon name="heart" size={20} color="#f72d60" solid={hasLiked} />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              onCommentClick(post);
            }}
            style={styles.Comment}>
            <Icon name="comment" size={20} color="#f72d60" solid={false} />
          </TouchableOpacity>
        </View>
        <View style={styles.AdditionalInfoContainer}>
          <Text style={styles.LikesCount}>
            {post.users_that_liked.length} Likes
          </Text>
          <Text style={styles.CommentCount}>
            {post.comments.length} Comments
          </Text>
        </View>
      </Card>
    </LinearGradient>
  );
}

const styles = StyleSheet.create({
  CardTopItemsContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    position: 'absolute',
    right: 0,
    zIndex: 1,
  },
  PostCard: {
    backgroundColor: 'transparent',
    margin: 0,
  },
  PostTitle: {textAlign: 'left', fontSize: 24},
  PostContentContainer: {},
  PostContent: {fontSize: 16},
  PostImage: {
    height: 200,
    width: '100%',
    marginVertical: 10,
  },
  PostLikeAndCommentContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  Like: {height: 20, width: 20, marginHorizontal: 10},
  Comment: {height: 20, width: 20},
  EllipsisContainer: {
    position: 'relative',
    right: 0,
    alignItems: 'flex-end',
  },
  EllipsisTouchableOpacity: {
    width: 20,
  },
  AdditionalInfoContainer: {
    alignItems: 'flex-end',
  },
  LikesCount: {fontSize: 12, marginHorizontal: 0},
  CommentCount: {fontSize: 12, marginHorizontal: 0},
  FlagContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  FlagText: {fontSize: 14, color: 'white'},
  LinearGradient: {
    flex: 1,
    borderRadius: 10,
    marginHorizontal: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
    width: '95%',
    alignSelf: 'center',
    marginVertical: 5,
  },
});

export default PostListItem;
