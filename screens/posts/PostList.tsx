import React, {useContext, useEffect, useRef, useState} from 'react';
import {
  StyleSheet,
  View,
  Text,
  ActivityIndicator,
  Alert,
  RefreshControl,
  Modal,
} from 'react-native';
import {
  BottomSheet,
  Button,
  Card,
  Input,
  ListItem,
  Overlay,
} from 'react-native-elements';
import TPost from '../../types/TPost';
import {ScrollView} from 'react-native-gesture-handler';

import PostListItem from './PostListItem';
import PostFlagForm from './PostFlagForm';
import {PostsContext} from '../../contexts/PostsContext';
import {AuthContext, ErrorContext} from '../../contexts';

import usePostListBottomSheetOptions, {
  EWhatMustBeDisplayed,
} from '../../hooks/usePostListBottomSheetOptions';

import {
  likePost,
  unlikePost,
  deletePost,
  flagPostInappropriate,
  removeFlag,
  voteInFavorOfFlag,
  revokeVote,
  postComment,
} from '../../api/post';
import {ParentDataContext} from '../../contexts/ParentDataContext';
import MessageAlert, {MessageAlertType} from '../../utils/MessageAlert';
import PostCommentsComponent from './postCommentsComponent/PostCommentsComponent';

function PostList(props: any) {
  const {posts, setPosts, isPostsLoading} = useContext(PostsContext);
  const {loggedInUser} = useContext(AuthContext);
  const {setShouldPostsDataUpdate} = useContext(ParentDataContext);
  const page = useRef<number>(0);
  const [isBottomSheetVisible, setIsBottomSheetVisible] = useState<boolean>(
    false,
  );
  const [isCommentsModalVisible, setIsCommentsModelVisible] = useState<boolean>(
    false,
  );
  const [
    isPostFlagReasonFormOverlayVisible,
    setIsPostFlagReasonFormOverlayVisible,
  ] = useState<boolean>(false);
  const [isPostActionLoading, setIsPostActionLoading] = useState<boolean>(
    false,
  );
  const [
    isSavingPostCommentLoading,
    setIsSavingPostCommentLoading,
  ] = useState<boolean>(false);
  const [selectedPost, setSelectedPost] = useState<TPost | null>(null);
  const {handleError} = useContext(ErrorContext);

  const whatMustBeDisplayed = usePostListBottomSheetOptions(
    selectedPost,
    loggedInUser,
  );

  const handleElipsisClick = (post: TPost): void => {
    setSelectedPost(post);
    setIsBottomSheetVisible(true);
  };

  useEffect(() => {
    const updatedPost = posts.find((post) => post._id === selectedPost?._id);
    if (updatedPost) {
      setSelectedPost(updatedPost);
    }
  }, [posts]);

  const handleLikeClick = async (post: TPost): Promise<void> => {
    try {
      const res = await likePost(post._id);
      setPosts((prev) =>
        prev.map((prevPost) => {
          if (prevPost._id === post._id) {
            return res.data.post;
          }
          return prevPost;
        }),
      );
    } catch (err) {
      handleError(err);
    }
  };

  const handleUnlikeClick = async (post: TPost): Promise<void> => {
    try {
      const res = await unlikePost(post._id);
      setPosts((prev) =>
        prev.map((prevPost) => {
          if (prevPost._id === post._id) {
            return res.data.post;
          }
          return prevPost;
        }),
      );
    } catch (err) {
      handleError(err);
    }
  };

  const handleCommentClick = (post: TPost): void => {
    setSelectedPost(post);
    setIsCommentsModelVisible(true);
  };

  const handeCommentSubmit = async (data: {content: string}, actions: any): Promise<void> => {
    try {
      setIsSavingPostCommentLoading(true);
      if (selectedPost) await postComment(selectedPost?._id, data);
      setShouldPostsDataUpdate(true);
      actions.resetForm();
    } catch (err) {
      handleError(err);
    } finally {
      setIsSavingPostCommentLoading(false);
    }
  };

  const deleteConfirmation = () => {
    Alert.alert(
      'Leave Confirmation',
      `Are you sure you want to delete`,
      [
        {
          text: 'Cancel',
          style: 'cancel',
        },
        {text: 'Yes, I am sure', onPress: () => handlePostDelete()},
      ],
      {cancelable: true},
    );
  };

  const handlePostDelete = async (): Promise<void> => {
    try {
      setIsPostActionLoading(true);
      if (selectedPost) await deletePost(selectedPost._id);
      setShouldPostsDataUpdate(true);
      MessageAlert(`Post successfully deleted`, MessageAlertType.SUCCESS);
    } catch (err) {
      handleError(err);
    } finally {
      setIsPostActionLoading(false);
      setIsBottomSheetVisible(false);
    }
  };

  const handleScroll = ({nativeEvent}: any) => {
    const bottom =
      nativeEvent.layoutMeasurement.height + nativeEvent.contentOffset.y >=
      nativeEvent.contentSize.height - 5;
    if (bottom) {
      page.current += 1;
    }
  };

  const handleInappropriateFlagging = async (reason: string) => {
    try {
      if (selectedPost) await flagPostInappropriate(selectedPost._id, reason);
      setIsPostActionLoading(true);
      setShouldPostsDataUpdate(true);
      MessageAlert(
        `Post successfully flagged inappropriate.`,
        MessageAlertType.SUCCESS,
      );
    } catch (err) {
      handleError(err);
    } finally {
      setIsPostActionLoading(false);
      setIsPostFlagReasonFormOverlayVisible(false);
    }
  };

  const flagRemoveConfirmation = async () => {
    Alert.alert(
      'Reason',
      'Are you sure you want to revoke your inappropriate flag from the post',
      [
        {
          text: 'Cancel',
          style: 'cancel',
        },
        {text: 'Yes, I am sure', onPress: handleFlagRevoke},
      ],
      {cancelable: true},
    );
  };

  const handleFlagRevoke = async () => {
    try {
      setIsPostActionLoading(true);
      if (selectedPost) {
        await removeFlag(selectedPost._id, selectedPost?.flags[0]._id);
        MessageAlert(`Flag successfully revoked.`, MessageAlertType.SUCCESS);
        setShouldPostsDataUpdate(true);
      }
    } catch (err) {
      handleError(err);
    } finally {
      setIsPostActionLoading(false);
      setIsBottomSheetVisible(false);
    }
  };

  const handleFlagVote = async () => {
    try {
      setIsPostActionLoading(true);
      if (selectedPost) {
        await voteInFavorOfFlag(selectedPost._id, selectedPost.flags[0]._id);
        MessageAlert(`Vote succesfully casted.`, MessageAlertType.SUCCESS);
        setShouldPostsDataUpdate(true);
      }
    } catch (err) {
      handleError(err);
    } finally {
      setIsBottomSheetVisible(false);
      setIsPostActionLoading(false);
    }
  };

  const handleFlagVoteRevoke = async () => {
    try {
      setIsPostActionLoading(true);
      if (selectedPost) {
        await revokeVote(selectedPost._id, selectedPost.flags[0]._id);
        MessageAlert(`Vote succesfully revoked.`, MessageAlertType.SUCCESS);
        setShouldPostsDataUpdate(true);
      }
    } catch (err) {
      handleError(err);
    } finally {
      setIsPostActionLoading(false);
      setIsBottomSheetVisible(false);
    }
  };

  const handleFlagBadgePress = (flag: {reason: string}) => {
    logoutConfirmation(flag.reason);
  };

  const logoutConfirmation = (reason: string) => {
    Alert.alert(
      'Reason',
      reason,
      [
        {
          text: 'Cancel',
          style: 'cancel',
        },
      ],
      {cancelable: true},
    );
  };

  return (
    <>
      <BottomSheet
        isVisible={isBottomSheetVisible}
        modalProps={{}}
        containerStyle={{
          backgroundColor: 'rgba(0.5, 0.25, 0, 0.2)',
        }}>
        {whatMustBeDisplayed === EWhatMustBeDisplayed.DELETE && (
          <ListItem
            containerStyle={{backgroundColor: 'red'}}
            onPress={deleteConfirmation}
            disabled={isPostActionLoading}>
            <ListItem.Content style={styles.BottomSheetListItemContent}>
              <ListItem.Title style={{color: 'white'}}>Delete</ListItem.Title>
              {isPostActionLoading && (
                <ActivityIndicator size="small" color="white" />
              )}
            </ListItem.Content>
          </ListItem>
        )}
        {whatMustBeDisplayed === EWhatMustBeDisplayed.REMOVE && (
          <ListItem
            containerStyle={{backgroundColor: 'red'}}
            onPress={deleteConfirmation}
            disabled={isPostActionLoading}>
            <ListItem.Content style={styles.BottomSheetListItemContent}>
              <ListItem.Title style={{color: 'white'}}>Remove</ListItem.Title>
              {isPostActionLoading && (
                <ActivityIndicator size="small" color="white" />
              )}
            </ListItem.Content>
            <Text style={{color: 'white'}}>
              Vote Count: {selectedPost?.flags[0].votes.length}
            </Text>
          </ListItem>
        )}
        {whatMustBeDisplayed === EWhatMustBeDisplayed.REVOKE_VOTE && (
          <ListItem
            disabled={isPostActionLoading}
            onPress={handleFlagVoteRevoke}>
            <ListItem.Content style={styles.BottomSheetListItemContent}>
              <ListItem.Title>Revoke your vote</ListItem.Title>
              {isPostActionLoading && (
                <ActivityIndicator size="small" color="whgreyite" />
              )}
            </ListItem.Content>
            <Text>Vote Count: {selectedPost?.flags[0].votes.length}</Text>
          </ListItem>
        )}
        {whatMustBeDisplayed === EWhatMustBeDisplayed.VOTE && (
          <ListItem disabled={isPostActionLoading} onPress={handleFlagVote}>
            <ListItem.Content style={styles.BottomSheetListItemContent}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <ListItem.Title>Vote inappropriate</ListItem.Title>
                {isPostActionLoading && (
                  <ActivityIndicator size="small" color="grey" />
                )}
              </View>
            </ListItem.Content>
            <Text>Vote Count: {selectedPost?.flags[0].votes.length}</Text>
          </ListItem>
        )}
        {whatMustBeDisplayed === EWhatMustBeDisplayed.FLAG && (
          <ListItem
            onPress={() => {
              setIsBottomSheetVisible(false);
              setIsPostFlagReasonFormOverlayVisible(true);
            }}>
            <ListItem.Content style={styles.BottomSheetListItemContent}>
              <ListItem.Title>Flag as unappropriate</ListItem.Title>
              {isPostActionLoading && (
                <ActivityIndicator size="small" color="grey" />
              )}
            </ListItem.Content>
          </ListItem>
        )}
        {whatMustBeDisplayed === EWhatMustBeDisplayed.REVOKE_FLAG && (
          <ListItem
            onPress={flagRemoveConfirmation}
            disabled={isPostActionLoading}>
            <ListItem.Content style={styles.BottomSheetListItemContent}>
              <ListItem.Title>Revoke flag</ListItem.Title>
              {isPostActionLoading && (
                <ActivityIndicator size="small" color="grey" />
              )}
            </ListItem.Content>
          </ListItem>
        )}
        <ListItem
          disabled={isPostActionLoading}
          containerStyle={{backgroundColor: 'grey'}}
          onPress={() => setIsBottomSheetVisible(false)}>
          <ListItem.Content>
            <ListItem.Title style={{color: 'white'}}>Cancel</ListItem.Title>
          </ListItem.Content>
        </ListItem>
      </BottomSheet>
      <Modal
        animationType="slide"
        transparent={true}
        visible={isCommentsModalVisible}>
        {selectedPost && (
          <PostCommentsComponent
            post={selectedPost}
            onClose={() => {
              setIsCommentsModelVisible(false);
            }}
            onSubmit={handeCommentSubmit}
            isSavingPostCommentLoading={isSavingPostCommentLoading}
          />
        )}
      </Modal>
      <Overlay
        isVisible={isPostFlagReasonFormOverlayVisible}
        onBackdropPress={() => setIsPostFlagReasonFormOverlayVisible(false)}>
        <PostFlagForm
          handleSubmit={handleInappropriateFlagging}
          isPostFlagLoading={isPostActionLoading}
        />
      </Overlay>
      <ScrollView
        onScroll={handleScroll}
        refreshControl={
          <RefreshControl
            refreshing={isPostsLoading}
            onRefresh={() => setShouldPostsDataUpdate(true)}
          />
        }>
        <View style={styles.PostListContainer}>
          {posts.map((p: TPost, index) => {
            return (
              <PostListItem
                post={p}
                key={p._id}
                onEllipsisClick={handleElipsisClick}
                loggedInUserId={loggedInUser?._id || ''}
                onLikeClick={handleLikeClick}
                onUnlikeClick={handleUnlikeClick}
                onCommentClick={handleCommentClick}
                handleFlagBadgePress={handleFlagBadgePress}
                index={index}
              />
            );
          })}
        </View>
        {!posts.length && !isPostsLoading && (
          <View style={styles.EmptyMessageContainer}>
            <Text style={styles.EmptyMessage}>No Results</Text>
          </View>
        )}
      </ScrollView>
    </>
  );
}

const styles = StyleSheet.create({
  PostListContainer: {marginBottom: '30%'},
  ActivityIndicatorView: {marginTop: 10},
  EmptyMessageContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: 64,
    padding: 40,
  },
  EmptyMessage: {
    fontSize: 32,
  },
  BottomSheetListItemContent: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
});

export default PostList;
