import React, {useState} from 'react';
import {StyleSheet, Text, TextInput, View} from 'react-native';
import {Button} from 'react-native-elements';

type TPostFlagFormProps = {
  handleSubmit: (reason: string) => void;
  isPostFlagLoading: boolean;
}

const PostFlagForm = ({handleSubmit, isPostFlagLoading}: TPostFlagFormProps) => {
  const [contentHeight, setContentHeight] = useState<number>(40);
  const [reason, setReason] = useState<string>('');
  const [
    displayValidationError,
    setDisplayValidationError,
  ] = useState<boolean>(false);

  const onSubmitPress = () => {
    if(!reason){
      setDisplayValidationError(true);
    }else{
      setDisplayValidationError(false);
      handleSubmit(reason);
    }
  };

  return (
    <>
      <View style={styles.InputView}>
        <Text style={styles.InputLabel}>Type your reason</Text>
        <TextInput
          onChangeText={(value) => setReason(value)}
          value={reason}
          style={[styles.ContentTextInput, {height: contentHeight}]}
          placeholderTextColor="#86939e"
          placeholder="Enter post content"
          onContentSizeChange={(e) =>
            setContentHeight(e.nativeEvent.contentSize.height)
          }
          editable={true}
          multiline={true}
        />
        <Text style={styles.InputValidationError}>
          {displayValidationError
            ? 'Reason for flagging the post is required'
            : ''}
        </Text>
        <Button title="submit" onPress={onSubmitPress} loading={isPostFlagLoading} disabled={isPostFlagLoading}/>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  InputView: {
    width: 300,
  },
  InputLabel: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#87949f',
    marginHorizontal: 10,
  },
  ContentTextInput: {
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderLeftWidth: 0,
    borderBottomWidth: 1,
    borderColor: '#87949f',
    borderWidth: 1,
    marginHorizontal: 10,
    fontSize: 18,
  },
  InputValidationError: {
    marginHorizontal: 10,
    color: '#dc3545',
    marginBottom: 5
  },
});

export default PostFlagForm;
