import React, {useEffect, useState} from 'react';
import {StyleSheet, View, Text, RefreshControl} from 'react-native';
import {ListItem, BottomSheet, Badge, SearchBar} from 'react-native-elements';
import {ScrollView} from 'react-native-gesture-handler';

// Type
import TMeetup from '../../types/TMeetup';
import {TLoggedInUser} from '../../contexts/AuthContext';
import LinearGradient from 'react-native-linear-gradient';
import {colorGradients} from '../../GlobalStyles';

type TMeetupsListParams = {
  navigation: any;
  meetups: Array<TMeetup>;
  isMeetupsLoading: boolean;
  handleRefresh: () => void;
  handleScroll: (e: any) => void;
  handleJoin: (meetup: TMeetup) => void;
  handleCancel: (meetup: TMeetup) => void;
  loggedInUser: TLoggedInUser;
};

const MeetupsList = ({
  meetups,
  isMeetupsLoading,
  handleScroll,
  handleRefresh,
  loggedInUser,
  handleJoin,
  handleCancel,
}: TMeetupsListParams) => {
  const [pressedMeetup, setPressedMeetup] = useState<TMeetup>();
  const [isBottomSheetVisible, setIsBottomSheetVisible] = useState<boolean>(
    false,
  );
  const [isConvener, setIsConvener] = useState(false);

  useEffect(() => {
    if (pressedMeetup?.convener_id === loggedInUser._id) {
      setIsConvener(true);
    } else {
      setIsConvener(false);
    }
  }, [pressedMeetup]);

  const handleMeetupPress = (meetup: TMeetup) => {
    setPressedMeetup(meetup);
    setIsBottomSheetVisible(true);
  };

  const handleBottomSheetCancelPress = () => {
    setIsBottomSheetVisible(false);
  };

  return (
    <>
      <BottomSheet
        isVisible={isBottomSheetVisible}
        modalProps={{}}
        containerStyle={{
          backgroundColor: 'rgba(0.5, 0.25, 0, 0.2)',
        }}>
        {isConvener && (
          <ListItem
            onPress={() => {
              setIsBottomSheetVisible(false);
              if (pressedMeetup) handleCancel(pressedMeetup);
            }}>
            <ListItem.Content>
              <ListItem.Title>Cancel Meetup</ListItem.Title>
            </ListItem.Content>
          </ListItem>
        )}
        <ListItem
          onPress={() => {
            setIsBottomSheetVisible(false);
            if (pressedMeetup) return handleJoin(pressedMeetup);
          }}>
          <ListItem.Content>
            <ListItem.Title>Join Meetup</ListItem.Title>
          </ListItem.Content>
        </ListItem>
        <ListItem
          onPress={handleBottomSheetCancelPress}
          containerStyle={{backgroundColor: 'grey'}}>
          <ListItem.Content>
            <ListItem.Title style={{color: 'white'}}>Cancel</ListItem.Title>
          </ListItem.Content>
        </ListItem>
      </BottomSheet>
      <ScrollView
        onScroll={handleScroll}
        refreshControl={
          <RefreshControl
            refreshing={isMeetupsLoading}
            onRefresh={handleRefresh}
          />
        }>
        <View style={styles.MeetupsScreenContentContainer}>
          {!!meetups.length && (
            <View>
              {meetups.map((meetup, i) => (
                <LinearGradient
                  colors={colorGradients[i % colorGradients.length]}
                  start={{x: 0, y: 0}}
                  end={{x: 1, y: 0}}
                  style={styles.LinearGradient}
                  key={`meetup-${i}`}>
                  <ListItem
                    key={i}
                    bottomDivider
                    containerStyle={
                      meetup.cancelled
                        ? styles.MeetupListItemCancelled
                        : styles.MeetupListItemActive
                    }
                    disabled={meetup.cancelled}
                    underlayColor="none"
                    onPress={() => handleMeetupPress(meetup)}>
                    <ListItem.Content>
                      {meetup.cancelled && (
                        <View style={styles.CancelledBadgeContainer}>
                          <Badge
                            value="Cancelled"
                            status="warning"
                            badgeStyle={styles.CancelledBadge}
                            textStyle={styles.CancelledBadgeText}
                          />
                        </View>
                      )}
                      <ListItem.Title>{meetup.channel_name}</ListItem.Title>
                      <ListItem.Subtitle>{meetup.purpose}</ListItem.Subtitle>
                      <View style={styles.AdditionalInfoContainer}>
                        <Text style={styles.AdditionalInfo}>
                          Date: {meetup.start_time.split('T')[0]}
                        </Text>
                        <Text style={styles.AdditionalInfo}>
                          Time: {meetup.start_time.split('T')[1].slice(0, 5)}
                        </Text>
                      </View>
                    </ListItem.Content>
                  </ListItem>
                </LinearGradient>
              ))}
            </View>
          )}
          {!meetups.length && !isMeetupsLoading && (
            <View style={styles.EmptyMessageContainer}>
              <Text style={styles.EmptyMessage}>No Results</Text>
            </View>
          )}
        </View>
      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  CancelledBadgeContainer: {
    position: 'absolute',
    top: 0,
    right: 0,
  },
  CancelledBadge: {
    padding: 10,
    marginHorizontal: 10,
    height: 30,
  },
  CancelledBadgeText: {
    fontSize: 14,
    color: '#1c1e21',
  },
  ButtonContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  Button: {
    margin: 5,
    borderRadius: 40,
    width: 150,
    paddingHorizontal: 0,
    backgroundColor: '#8086F2',
  },
  ButtonTitle: {
    fontSize: 10,
  },
  ButtonIcon: {
    padding: 5,
  },
  MeetupListItemActive: {backgroundColor: 'transparent'},
  MeetupListItemCancelled: {opacity: 0.6},
  ActivityIndicatorView: {marginTop: 10},
  EmptyMessageContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: 64,
    padding: 40,
  },
  EmptyMessage: {
    fontSize: 32,
  },
  MeetupsScreenContentContainer: {
    marginBottom: '30%',
  },
  AdditionalInfoContainer: {
    alignItems: 'flex-end',
    width: '100%',
  },
  AdditionalInfo: {
    fontSize: 12,
    fontStyle: 'italic',
  },
  LinearGradient: {
    flex: 1,
    borderRadius: 10,
    marginHorizontal: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
    width: '95%',
    alignSelf: 'center',
    marginVertical: 5,
  },
});

export default MeetupsList;
