import React, {useState, useContext, useEffect, useRef} from 'react';
import {Alert, Platform} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import ImagePicker from 'react-native-image-crop-picker';
import {ErrorContext} from '../../contexts/ErrorContext';
import {MeetupsContext} from '../../contexts/MeetupsContext';

import {createMeetup, updateMeetup} from '../../api/meetup';
import MessageAlert, {MessageAlertType} from '../../utils/MessageAlert';
import StackScreenHeader from '../../components/StackScreenHeader';

// Screens
import MeetupsList from './MeetupsList';
import CreateMeetupScreen from './CreateMeetupScreen';

// Types
import TMeetup from '../../types/TMeetup';
import {ParentDataContext} from '../../contexts/ParentDataContext';
import {AuthContext} from '../../contexts';
import MeetupVideoScreen from './MeetupVideoScreen';

type TMeetupParams = {
  navigation: any;
};

const MeetupsScreen = ({navigation}: TMeetupParams) => {
  const {meetups, isMeetupsLoading, setShouldLoadMore} = useContext(
    MeetupsContext,
  );
  const [selectedMeetup, setSelectedMeetup] = useState<TMeetup>();
  const [
    shouldNavigateToMeetupVideo,
    setShouldNavigateToMeetupVideo,
  ] = useState<boolean>(false);
  const [isCreateLoading, setIsCreateLoading] = useState<boolean>(false);
  const [isCancelLoading, setIsCancelLoading] = useState<boolean>(false);

  const {loggedInUser} = useContext(AuthContext);

  const {setShouldMeetupsDataUpdate} = useContext(ParentDataContext);
  const {handleError} = useContext(ErrorContext);
  const Stack = createStackNavigator();

  useEffect(() => {
    if (shouldNavigateToMeetupVideo) {
      navigation.navigate('Meetup Video');
      setShouldNavigateToMeetupVideo(false);
    }
  }, [shouldNavigateToMeetupVideo]);

  const handleSubmit = async (data: any, actions: any): Promise<any> => {
    try {
      setIsCreateLoading(true);
      await createMeetup(data);
      MessageAlert('Meetup successfully created', MessageAlertType.SUCCESS);
      actions.resetForm();
      navigation.navigate('Meeting List');
    } catch (err) {
      setIsCreateLoading(false);
      handleError(err);
    }
  };

  const handleJoin = async (meetup: TMeetup) => {
    setSelectedMeetup(meetup);
    setShouldNavigateToMeetupVideo(true);
  };

  const handleLeave = async (meetup: TMeetup) => {};

  const handleCancel = async (meetup: TMeetup) => {
    try {
      setIsCancelLoading(true);
      await updateMeetup(meetup._id, {cancelled: true});
      MessageAlert('Meetup successfully cancelled', MessageAlertType.SUCCESS);
      setShouldMeetupsDataUpdate(true);
    } catch (err) {
      handleError(err);
    } finally {
      setIsCancelLoading(false);
    }
  };

  const handleScroll = ({nativeEvent}: any) => {
    const bottom =
      nativeEvent.layoutMeasurement.height + nativeEvent.contentOffset.y >=
      nativeEvent.contentSize.height - 5;
    if (bottom) {
      setShouldLoadMore(true);
    }
  };

  const leaveConfirmation = (meetup: TMeetup) => {
    Alert.alert(
      'Leave Confirmation',
      `Are you sure you want to leave ${meetup.channel_name}`,
      [
        {
          text: 'Cancel',
          style: 'cancel',
        },
        {text: 'Yes, I am sure', onPress: () => handleLeave(meetup)},
      ],
      {cancelable: true},
    );
  };

  return (
    <>
      <Stack.Navigator initialRouteName="Meetup List">
        <Stack.Screen
          name="Meetup List"
          options={{
            header: (props) => <StackScreenHeader text="Meetups" {...props} />,
          }}>
          {(props) =>
            !!loggedInUser && (
              <MeetupsList
                meetups={meetups}
                isMeetupsLoading={isMeetupsLoading}
                handleScroll={handleScroll}
                handleRefresh={() => setShouldMeetupsDataUpdate(true)}
                loggedInUser={loggedInUser}
                handleJoin={handleJoin}
                handleCancel={handleCancel}
                {...props}
              />
            )
          }
        </Stack.Screen>
        <Stack.Screen
          name="Create Meetup"
          options={{
            header: (props) => (
              <StackScreenHeader
                text="Organize Meetup"
                enableReturn={true}
                {...props}
              />
            ),
          }}>
          {(props) => <CreateMeetupScreen {...props} />}
        </Stack.Screen>
        {selectedMeetup && (
          <Stack.Screen
            name="Meetup Video"
            options={{
              header: (props) => (
                <StackScreenHeader
                  text="Meetup"
                  enableReturn={true}
                  {...props}
                />
              ),
            }}>
            {(props) => (
              <MeetupVideoScreen
                channelName={selectedMeetup?.channel_name}
                {...props}
              />
            )}
          </Stack.Screen>
        )}
      </Stack.Navigator>
    </>
  );
};

export default MeetupsScreen;
