import React, {useContext, useState} from 'react';
import {
  StyleSheet,
  View,
  KeyboardAvoidingView,
  TouchableOpacity,
  ScrollView,
  Platform,
} from 'react-native';
import {Input, Button, Card} from 'react-native-elements';
import {Formik} from 'formik';
import DateTimePicker from '@react-native-community/datetimepicker';
import {createMeetup} from '../../api/meetup';
import MessageAlert, {MessageAlertType} from '../../utils/MessageAlert';
import {ErrorContext} from '../../contexts';
import {ParentDataContext} from '../../contexts/ParentDataContext';

type TMeetupForm = {
  community_id: string;
  channel_name: string;
  purpose: string;
  start_date: string;
  start_time: string;
  duration: string;
};

type TCreateMeetupParams = {
  route: any;
  navigation: any;
};

function MeetupForm({route, navigation}: TCreateMeetupParams) {
  const initialValues: TMeetupForm = {
    community_id: route.params.community._id,
    channel_name: route.params.community.name,
    purpose: '',
    start_date: '',
    start_time: '',
    duration: '',
  };
  const [isCreateLoading, setIsCreateLoading] = useState(false);
  const [startDate, setStartDate] = useState<string>();
  const [startTime, setStartTime] = useState<string>();
  const [dateTimePickerMode, setDateTimePickerMode] = useState<string>('date');
  const [showDateTimePicker, setShowDateTimePicker] = useState<boolean>(false);
  const {setShouldMeetupsDataUpdate} = useContext(ParentDataContext);
  const {handleError} = useContext(ErrorContext);

  const onDateChange = (event: Event, selectedDate: Date | undefined) => {
    const currentDate = selectedDate?.toISOString() || startDate;
    setShowDateTimePicker(Platform.OS === 'ios');
    setStartDate(currentDate?.split('T')[0]);
    setShowDateTimePicker(false);
  };

  const onTimeChange = (event: Event, selectedTime: Date | undefined) => {
    const currentDate = selectedTime?.toISOString() || startTime;
    setShowDateTimePicker(Platform.OS === 'ios');
    setStartTime(currentDate?.split('T')[1].slice(0, 7));
    setShowDateTimePicker(false);
  };

  const showMode = (currentMode: string) => {
    setShowDateTimePicker(true);
    setDateTimePickerMode(currentMode);
  };

  const showDatepicker = () => {
    showMode('date');
  };

  const showTimepicker = () => {
    showMode('time');
  };

  const handleSubmit = async (data: any, actions: any): Promise<any> => {
    try {
      setIsCreateLoading(true);
      await createMeetup(data);
      MessageAlert('Meetup successfully created', MessageAlertType.SUCCESS);
      actions.resetForm();
      setShouldMeetupsDataUpdate(true);
      navigation.navigate('Meetup', {
        screen: 'Meetup List',
      });
    } catch (err) {
      setIsCreateLoading(false);
      handleError(err);
    } finally {
      setIsCreateLoading(false);
    }
  };

  return (
    <ScrollView>
      <View style={styles.MeetupFormContainer}>
        <Formik
          initialValues={initialValues}
          onSubmit={(values, actions) => {
            if (startTime && startDate) {
              handleSubmit(
                {
                  ...values,
                  start_time: Date.parse(`${startDate} ${startTime}`),
                },
                actions,
              );
            }
          }}>
          {({handleChange, handleBlur, handleSubmit, values, touched}) => (
            <View style={styles.MeetupForm}>
              <KeyboardAvoidingView behavior="position">
                <Card containerStyle={styles.CommunityFormForm}>
                  <View style={styles.InputView}>
                    <Input
                      label="Purpose for meeting"
                      placeholder="Enter meeting purpose"
                      onChangeText={handleChange('purpose')}
                      onBlur={handleBlur('purpose')}
                      value={values.purpose}
                    />
                  </View>
                  <TouchableOpacity onPress={showDatepicker}>
                    <View style={styles.InputView}>
                      <Input
                        label="Start Date"
                        placeholder="Select start date"
                        disabled={true}
                        value={startDate}
                        errorMessage={
                          !startDate && touched.start_date ? 'required' : ''
                        }
                      />
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={showTimepicker}>
                    <View style={styles.InputView}>
                      <Input
                        label="Start time"
                        placeholder="Select start time"
                        disabled={true}
                        value={startTime}
                        errorMessage={
                          !startTime && touched.start_time ? 'required' : ''
                        }
                      />
                    </View>
                  </TouchableOpacity>
                  {showDateTimePicker && (
                    <DateTimePicker
                      testID="dateTimePicker"
                      value={
                        startDate && startTime
                          ? new Date(`${startDate}T${startTime}`)
                          : new Date(Date.now())
                      }
                      mode={dateTimePickerMode === 'date' ? 'date' : 'time'}
                      is24Hour={true}
                      display="default"
                      onChange={
                        dateTimePickerMode === 'date'
                          ? onDateChange
                          : onTimeChange
                      }
                    />
                  )}
                  <View style={styles.InputView}>
                    <Input
                      label="Duration"
                      placeholder="Give an approximation"
                      onBlur={handleBlur('duration')}
                      onChangeText={handleChange('duration')}
                      value={values.duration}
                    />
                  </View>
                  <View style={styles.MeetupFormBtnContainer}>
                    <Button
                      title="Save"
                      onPress={handleSubmit}
                      loading={isCreateLoading}
                      disabled={isCreateLoading}
                      disabledStyle={styles.DisabledButton}
                    />
                  </View>
                </Card>
              </KeyboardAvoidingView>
            </View>
          )}
        </Formik>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  MeetupForm: {
    width: '80%',
    marginVertical: 10
  },
  MeetupFormContainer: {
    alignItems: 'center',
  },

  image: {
    height: 150,
    width: 150,
  },

  InputView: {
    width: '100%',
    height: 40,
    marginBottom: 50,
    alignItems: 'center',
  },

  CommunityFormForm: {
    borderRadius: 10,
    marginHorizontal: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },

  MeetupFormBtnContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    width: '100%',
  },

  MeetupFormBtn: {
    width: 60,
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
    backgroundColor: '#ff6666',
    marginLeft: 10,
  },

  MeetupFormSigninContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 10,
  },

  CommunityFormText: {
    color: '#5eaaa8',
    textDecorationStyle: 'dashed',
  },

  InputIcon: {
    color: '#86939e',
  },

  logoView: {
    margin: 10,
  },

  CameraIcon: {
    position: 'absolute',
    bottom: '0%',
    right: '0%',
    fontSize: 16,
  },

  LogoSelector: {
    height: 100,
    width: 100,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#5253d9',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
    overflow: 'hidden',
  },

  logo: {
    height: '100%',
    width: '100%',
  },

  DisabledButton: {
    backgroundColor: '#94bee3',
  },

  LogoSelectorContainer: {
    alignItems: 'center',
  },

  PrivacyPicker: {height: 50, width: '100%'},
});

export default MeetupForm;
