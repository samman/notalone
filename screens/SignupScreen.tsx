import React, {useState, useContext} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ActivityIndicator,
  KeyboardAvoidingView,
  ScrollView,
  Image,
  Platform,
} from 'react-native';
import {Input, BottomSheet, ListItem} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Formik} from 'formik';
import * as Yup from 'yup';
import {signup} from '../api/auth';
import {uploadProfilePic} from '../api/user';
import AsyncStorage from '@react-native-async-storage/async-storage';
import ImagePicker from 'react-native-image-crop-picker';
import {Picker} from '@react-native-picker/picker';
import MessageAlert, {MessageAlertType} from '../utils/MessageAlert';

import {AuthContext, ErrorContext} from '../contexts';

type TSignupUser = {
  name: string;
  username: string;
  password: string;
  passwordConfirm: string;
  email: string;
  address: string;
  gender: Gender;
};

export enum Gender {
  male = 'male',
  female = 'female',
  other = 'other',
}

type TSignupParams = {
  navigation: any;
};

const SignupSchema = Yup.object().shape({
  name: Yup.string().required('Required'),
  username: Yup.string().required('Required'),
  email: Yup.string().email('Enter a valid email').required('Required'),
  password: Yup.string().required('Required'),
  passwordConfirm: Yup.string().required('Required'),
});

function SignupScreen({navigation}: TSignupParams) {
  const [securePassoword, setSecurePassword] = useState<boolean>(true);
  const passwordEyeIconName: string = securePassoword ? 'eye' : 'eye-slash';
  const [securePassowordConfirm, setSecurePasswordConfirm] = useState<boolean>(
    true,
  );
  const passwordConfirmEyeIconName: string = securePassowordConfirm
    ? 'eye'
    : 'eye-slash';
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const {handleError} = useContext(ErrorContext);
  const {setJwtToken} = useContext(AuthContext);
  const [ProfilePic, setProfilePic] = useState<{path: string; mime: string}>();
  const [isBottomSheetVisible, setIsBottomSheetVisible] = useState(false);

  const initialValues: TSignupUser = {
    name: '',
    username: '',
    password: '',
    passwordConfirm: '',
    email: '',
    address: '',
    gender: Gender.male,
  };

  const selectProfilePicFormGallery: () => void = (): void => {
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: true,
    }).then((image: any) => {
      setProfilePic(image);
    });
  };

  const captureProfilePic: () => void = (): void => {
    ImagePicker.openCamera({
      width: 300,
      height: 400,
      cropping: true,
    })
      .then((image: any) => {
        setProfilePic(image);
      })
      .catch((err: any) => {
        console.log(err);
      });
  };

  const handleSubmit = async (data: any): Promise<any> => {
    try {
      // Signing up the user
      setIsLoading(true);
      const res: any = await signup(data);
      await AsyncStorage.setItem('jwt_token', res.data.token);
      setJwtToken(res.data.token);
      // Uploading profile pic
      if (ProfilePic) {
        let formData = new FormData();
        formData.append('profilePic', {
          name: ProfilePic.path.split('/').pop(),
          type: ProfilePic.mime,
          uri:
            Platform.OS === 'android'
              ? ProfilePic.path
              : ProfilePic.path.replace('file://', ''),
        });
        await uploadProfilePic(res.data.token, formData);
      }
      MessageAlert('Signup Successful', MessageAlertType.SUCCESS);
    } catch (err) {
      handleError(err);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <KeyboardAvoidingView>
      <ScrollView>
        <View style={styles.SignupContainer}>
          <Formik
            initialValues={initialValues}
            validationSchema={SignupSchema}
            onSubmit={handleSubmit}>
            {({
              handleChange,
              handleBlur,
              handleSubmit,
              values,
              setFieldValue,
              errors,
              touched,
            }) => (
              <>
                <View style={styles.SignupForm}>
                  <BottomSheet
                    isVisible={isBottomSheetVisible}
                    modalProps={{}}
                    containerStyle={{
                      backgroundColor: 'rgba(0.5, 0.25, 0, 0.2)',
                    }}>
                    <ListItem
                      onPress={() => {
                        setIsBottomSheetVisible(false);
                        selectProfilePicFormGallery();
                      }}>
                      <ListItem.Content>
                        <ListItem.Title>Open galery</ListItem.Title>
                      </ListItem.Content>
                    </ListItem>
                    <ListItem
                      onPress={() => {
                        setIsBottomSheetVisible(false);
                        captureProfilePic();
                      }}>
                      <ListItem.Content>
                        <ListItem.Title>Open camera</ListItem.Title>
                      </ListItem.Content>
                    </ListItem>
                    <ListItem
                      containerStyle={{backgroundColor: 'grey'}}
                      onPress={() => setIsBottomSheetVisible(false)}>
                      <ListItem.Content>
                        <ListItem.Title style={{color: 'white'}}>
                          Cancel
                        </ListItem.Title>
                      </ListItem.Content>
                    </ListItem>
                  </BottomSheet>
                  <View style={styles.ProfilePicView}>
                    <TouchableOpacity
                      onPress={() => setIsBottomSheetVisible(true)}
                      style={styles.ProfilePicSelector}>
                      {ProfilePic ? (
                        <Image
                          source={{uri: ProfilePic.path}}
                          style={styles.ProfilePic}
                        />
                      ) : (
                        <Icon name="user" size={50} color="white" />
                      )}
                    </TouchableOpacity>
                    <Icon name="camera" color="red" style={styles.CameraIcon} />
                  </View>
                  <View style={styles.InputView}>
                    <Input
                      label="Full Name"
                      placeholder="Enter full name"
                      onChangeText={handleChange('name')}
                      onBlur={handleBlur('name')}
                      value={values.name}
                      errorMessage={
                        errors.name && touched.name ? errors.name : ''
                      }
                    />
                  </View>
                  <View style={styles.InputView}>
                    <Input
                      label="Username"
                      placeholder="Enter Username"
                      onChangeText={handleChange('username')}
                      onBlur={handleBlur('username')}
                      value={values.username}
                      errorMessage={
                        errors.username && touched.username
                          ? errors.username
                          : ''
                      }
                    />
                  </View>
                  <View style={styles.InputView}>
                    <Input
                      label="Email"
                      placeholder="Enter email"
                      onChangeText={handleChange('email')}
                      onBlur={handleBlur('email')}
                      value={values.email}
                      errorMessage={
                        errors.email && touched.email ? errors.email : ''
                      }
                    />
                  </View>
                  <View style={styles.InputView}>
                    <Input
                      label="Address"
                      placeholder="Enter address"
                      onChangeText={handleChange('address')}
                      onBlur={handleBlur('address')}
                      value={values.address}
                      errorMessage={
                        errors.address && touched.address ? errors.address : ''
                      }
                    />
                  </View>
                  <View style={styles.InputView}>
                    <Input
                      inputContainerStyle={styles.InputWithNoBorderBottom}
                      label="Gender"
                      placeholder="Enter gender constraint"
                      errorMessage={
                        errors.gender && touched.gender ? errors.gender : ''
                      }
                      disabled={true}
                      InputComponent={() => (
                        <Picker
                          selectedValue={values.gender}
                          style={styles.GenderPicker}
                          onValueChange={(itemValue) => {
                            setFieldValue('gender', itemValue);
                          }}>
                          <Picker.Item label="Male" value={Gender.male} />
                          <Picker.Item label="Female" value={Gender.female} />
                          <Picker.Item label="Other" value={Gender.other} />
                        </Picker>
                      )}
                    />
                  </View>
                  <View style={styles.InputView}>
                    <Input
                      secureTextEntry={securePassoword}
                      placeholder="Enter Password"
                      label="Password"
                      rightIcon={
                        <TouchableOpacity
                          onPress={() => setSecurePassword((prev) => !prev)}>
                          <Icon
                            name={passwordEyeIconName}
                            size={24}
                            color="black"
                          />
                        </TouchableOpacity>
                      }
                      onChangeText={handleChange('password')}
                      onBlur={handleBlur('password')}
                      value={values.password}
                      errorMessage={
                        errors.password && touched.password
                          ? errors.password
                          : ''
                      }
                    />
                  </View>
                  <View style={styles.InputView}>
                    <Input
                      secureTextEntry={securePassowordConfirm}
                      placeholder="Enter password again"
                      label="Confirm Password"
                      rightIcon={
                        <TouchableOpacity
                          onPress={() =>
                            setSecurePasswordConfirm((prev) => !prev)
                          }>
                          <Icon
                            name={passwordConfirmEyeIconName}
                            size={24}
                            color="black"
                          />
                        </TouchableOpacity>
                      }
                      onChangeText={handleChange('passwordConfirm')}
                      onBlur={handleBlur('passwordConfirm')}
                      value={values.passwordConfirm}
                      errorMessage={
                        errors.passwordConfirm && touched.passwordConfirm
                          ? errors.passwordConfirm
                          : ''
                      }
                    />
                  </View>
                  <View style={styles.SignupScreenBtnContainer}>
                    <Text>Sign up</Text>
                    <TouchableOpacity
                      style={styles.SignupScreenBtn}
                      onPress={handleSubmit}
                      disabled={isLoading}>
                      {isLoading ? (
                        <ActivityIndicator size="large" color="white" />
                      ) : (
                        <Icon name="arrow-right" size={15} color="white" />
                      )}
                    </TouchableOpacity>
                  </View>
                </View>

                <View style={styles.SignupScreenSigninContainer}>
                  <Text>Already a member? </Text>
                  <TouchableOpacity
                    onPress={() => navigation.navigate('Login')}>
                    <Text style={styles.SignupText}>Sign in</Text>
                  </TouchableOpacity>
                </View>
              </>
            )}
          </Formik>
        </View>
      </ScrollView>
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  SignupContainer: {
    width: '80%',
    marginLeft: '10%',
  },

  Image: {
    height: 150,
    width: 150,
  },

  InputView: {
    width: '100%',
    height: 40,
    marginBottom: 50,
    alignItems: 'center',
  },

  SignupForm: {
    alignItems: 'center',
    width: '100%',
  },

  SignupScreenBtnContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    width: '100%',
  },

  SignupScreenBtn: {
    width: 60,
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
    backgroundColor: '#ff6666',
    marginLeft: 10,
  },

  SignupScreenSigninContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: 10,
  },

  SignupText: {
    color: '#5eaaa8',
    textDecorationStyle: 'dashed',
  },

  InputIcon: {
    color: '#86939e',
  },

  ProfilePicView: {
    margin: 10,
  },

  CameraIcon: {
    position: 'absolute',
    bottom: '0%',
    right: '0%',
    fontSize: 16,
  },

  ProfilePicSelector: {
    height: 100,
    width: 100,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#5253d9',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    overflow: 'hidden',
    elevation: 4,
  },

  ProfilePic: {
    height: '100%',
    width: '100%',
  },
  InputWithNoBorderBottom: {
    borderBottomWidth: 0,
    backgroundColor: '#cbcbcb',
    borderRadius: 5,
  },
  GenderPicker: {height: 50, width: '100%'},
});

export default SignupScreen;
