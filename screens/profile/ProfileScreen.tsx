/***************************************************************************************
 *    Title: react-native-user-profile
 *    Author: Nattatorn Yucharoen
 *    Date: 28th April, 2021
 *    Availability: https://github.com/nattatorn-dev/react-native-user-profile/blob/master/screens/Profile1/Profile.js
 ***************************************************************************************/

import React, {useContext} from 'react';
import env from '../../environment';
import {
  Alert,
  FlatList,
  Image,
  ImageBackground,
  Platform,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {AuthContext} from '../../contexts';

import {Button, Card, Icon} from 'react-native-elements';
import {ScrollView} from 'react-native-gesture-handler';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import Separator from '../../components/Seperator';
import FlatListItem from '../../components/FlatListItem';

type TProfileParams = {
  navigation: any;
};

const ProfileScreen = ({navigation}: TProfileParams) => {
  const {loggedInUser, logout} = useContext(AuthContext);

  const logoutConfirmation = () => {
    Alert.alert(
      'Logout Confirmation',
      'Are you sure you want to logout',
      [
        {
          text: 'Cancel',
          style: 'cancel',
        },
        {text: 'Yes, I am sure', onPress: logout},
      ],
      {cancelable: true},
    );
  };

  return (
    <View style={styles.container}>
      <Card containerStyle={styles.cardContainer}>
        <View style={styles.headerContainer}>
          <ImageBackground
            style={styles.headerBackgroundImage}
            blurRadius={10}
            source={{uri: `${env.API_BASE_URL}/${loggedInUser?.profilePic}`}}>
            <View style={styles.headerColumn}>
              <Image
                style={styles.userImage}
                source={{
                  uri: `${env.API_BASE_URL}/${loggedInUser?.profilePic}`,
                }}
              />
              <Text style={styles.userNameText}>{loggedInUser?.name}</Text>
              <View style={styles.userAddressRow}>
                <View>
                  <FontAwesome5Icon
                    name="user"
                    solid={true}
                    style={styles.placeIcon}
                  />
                </View>
                <View style={styles.usernameRow}>
                  <Text style={styles.usernameText}>
                    {loggedInUser?.username}
                  </Text>
                </View>
              </View>
            </View>
          </ImageBackground>
        </View>
        {loggedInUser && (
          <FlatList
            contentContainerStyle={styles.FlatList}
            keyExtractor={(item, index) => `email-${index}`}
            data={[
              {text: loggedInUser.email, iconName: 'envelope'},
              {text: loggedInUser.address, iconName: 'map-marker'},
              {text: loggedInUser.gender, iconName: 'user'},
            ]}
            renderItem={({item}) => (
              <>
                <FlatListItem text={item.text} iconName={item.iconName} />
              </>
            )}></FlatList>
        )}
        <Button
          title="Logout"
          onPress={logoutConfirmation}
          buttonStyle={{backgroundColor: '#dc3545'}}
        />
      </Card>
    </View>
  );
};

const styles = StyleSheet.create({
  FlatList: {backgroundColor: '#FFF', paddingTop: 30},
  cardContainer: {
    backgroundColor: '#FFF',
    borderWidth: 0,
    flex: 1,
    margin: 0,
    padding: 0,
  },
  container: {
    flex: 1,
    marginBottom: 60,
  },
  headerBackgroundImage: {
    paddingBottom: 20,
    paddingTop: 45,
  },
  headerContainer: {},
  headerColumn: {
    backgroundColor: 'transparent',
    ...Platform.select({
      ios: {
        alignItems: 'center',
        elevation: 1,
        marginTop: -1,
      },
      android: {
        alignItems: 'center',
      },
    }),
  },
  placeIcon: {
    color: 'white',
    fontSize: 16,
    marginHorizontal: 5,
  },
  scroll: {
    backgroundColor: '#FFF',
  },
  userAddressRow: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  usernameRow: {
    backgroundColor: 'transparent',
  },
  userImage: {
    borderColor: '#FFF',
    borderRadius: 85,
    borderWidth: 3,
    height: 170,
    marginBottom: 15,
    width: 170,
  },
  usernameText: {
    color: '#FFF',
    fontSize: 22,
    fontWeight: 'bold',
    paddingBottom: 8,
    textAlign: 'center',
  },
  userNameText: {
    color: '#FFF',
    fontSize: 22,
    fontWeight: 'bold',
    paddingBottom: 8,
    textAlign: 'center',
  },
});

export default ProfileScreen;
