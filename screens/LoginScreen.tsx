import React, {useState, useContext} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import {Input} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Formik} from 'formik';
import * as Yup from 'yup';
import {login} from '../api/auth';
import AsyncStorage from '@react-native-async-storage/async-storage';
import MessageAlert, {MessageAlertType} from '../utils/MessageAlert';

//Error Context
import {ErrorContext, AuthContext} from '../contexts';

type TLoginParams = {
  navigation: any;
};

type TLoginForm = {
  username: string;
  password: string;
};

const LoginSchema = Yup.object().shape({
  username: Yup.string().required('Required'),
  password: Yup.string().required('Required'),
});

function LoginScreen({navigation}: TLoginParams) {
  const {handleError} = useContext(ErrorContext);
  const {setJwtToken} = useContext(AuthContext);
  const [securePassoword, setSecurePassword] = useState(true);
  const [isLoading, setIsLoading] = useState(false);
  const eyeIconName = securePassoword ? 'eye' : 'eye-slash';
  const initialValues: TLoginForm = {username: '', password: ''};

  const handleSubmit = async (formData: object) => {
    try {
      setIsLoading(true);
      const res: any = await login(formData);
      await AsyncStorage.setItem('jwt_token', res.data.token);
      setJwtToken(res.data.token);
      MessageAlert('Login Successful', MessageAlertType.SUCCESS);
    } catch (err) {
      handleError(err);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <>
      <Formik
        initialValues={initialValues}
        validationSchema={LoginSchema}
        onSubmit={(values) => handleSubmit(values)}>
        {({
          handleChange,
          handleBlur,
          handleSubmit,
          values,
          errors,
          touched,
        }) => (
          <View style={styles.LoginContainer}>
            <Image
              style={styles.image}
              source={require('../assets/logos/logo.png')}
            />

            <View style={styles.inputView}>
              <Input
                label="Username"
                placeholder="Enter Username"
                leftIcon={
                  <Icon name="user" size={24} style={styles.InputIcon} />
                }
                onChangeText={handleChange('username')}
                onBlur={handleBlur('username')}
                value={values.username}
                errorMessage={
                  errors.username && touched.username ? errors.username : ''
                }
              />
            </View>

            <View style={styles.inputView}>
              <Input
                secureTextEntry={securePassoword}
                placeholder="Enter Password"
                label="Password"
                leftIcon={
                  <Icon name="lock" size={24} style={styles.InputIcon} />
                }
                rightIcon={
                  <TouchableOpacity
                    onPress={() => setSecurePassword((prev) => !prev)}>
                    <Icon name={eyeIconName} size={24} color="black" />
                  </TouchableOpacity>
                }
                onChangeText={handleChange('password')}
                onBlur={handleBlur('password')}
                value={values.password}
                errorMessage={
                  errors.password && touched.password ? errors.password : ''
                }
              />
            </View>

            <View style={styles.LoginScreenBtnContainer}>
              <Text>Sign in</Text>
              <TouchableOpacity
                style={styles.LoginScreenBtn}
                onPress={handleSubmit}
                disabled={isLoading}>
                {isLoading ? (
                  <ActivityIndicator size="large" color="white" />
                ) : (
                  <Icon name="arrow-right" size={15} color="white" />
                )}
              </TouchableOpacity>
            </View>

            <View style={styles.LoginScreenSignupContainer}>
              <Text>Not a member? </Text>
              <TouchableOpacity onPress={() => navigation.navigate('Signup')}>
                <Text style={styles.SignupText}>Sign up</Text>
              </TouchableOpacity>
            </View>
          </View>
        )}
      </Formik>
    </>
  );
}

const styles = StyleSheet.create({
  LoginContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    width: '80%',
    marginLeft: '10%',
  },

  image: {
    height: 150,
    width: 150,
  },

  inputView: {
    width: '100%',
    height: 45,
    marginBottom: 50,
    alignItems: 'center',
  },

  forgot_button: {
    height: 30,
    marginBottom: 30,
  },

  LoginScreenBtnContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    width: '100%',
  },

  LoginScreenBtn: {
    width: 60,
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    borderRadius: 100,
    backgroundColor: '#ff6666',
    marginLeft: 10,
  },

  LoginScreenSignupContainer: {
    display: 'flex',
    flexDirection: 'row',
    marginTop: 100,
  },

  SignupText: {
    color: '#5eaaa8',
    textDecorationStyle: 'dashed',
  },

  InputIcon: {
    color: '#86939e',
  },

  ErrorMessageContainer: {
    margin: 20,
    backgroundColor: 'red',
  },
});

export default LoginScreen;
