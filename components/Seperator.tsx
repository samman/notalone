/***************************************************************************************
*    Title: react-native-user-profile
*    Author: Nattatorn Yucharoen
*    Date: 28th April, 2021
*    Availability: https://github.com/nattatorn-dev/react-native-user-profile/blob/master/screens/Profile1/Separator.js
***************************************************************************************/

import React from 'react';
import {StyleSheet, View} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  separatorOffset: {
    flex: 2,
    flexDirection: 'row',
  },
  separator: {
    borderColor: '#EDEDED',
    borderWidth: 0.8,
    flex: 8,
    flexDirection: 'row',
  },
});

const Separator = () => (
  <View style={styles.container}>
    <View style={styles.separatorOffset} />
    <View style={styles.separator} />
  </View>
);

export default Separator;
