import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Button} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';

type TStackScreenHeaderProps = {
  text: string;
  innerComponent?: React.FC;
  navigation: any;
  enableReturn?: boolean;
};

const StackScreenHeader = (props: TStackScreenHeaderProps) => (
  <View style={styles.HeaderContainer}>
    {props.enableReturn && (
      <Button
        icon={
          <Icon
            name="arrow-left"
            size={10}
            style={styles.BackButtonIcon}
            color="white"
          />
        }
        buttonStyle={styles.BackButton}
        onPress={() => {
          props.navigation.goBack();
        }}
      />
    )}
    <View style={styles.HeaderContentsContainer}>
      <Text style={styles.HeaderText}>{props.text}</Text>
      {!!props.innerComponent && props.innerComponent({})}
    </View>
  </View>
);

const styles = StyleSheet.create({
  HeaderContainer: {
    height: 60,
    flexDirection: 'row',
    backgroundColor: '#6960f7',
    alignItems: 'center',
  },
  HeaderContentsContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 15,
  },
  HeaderText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#ffff',
  },
  BackButtonIcon: {
    fontSize: 16,
  },
  BackButton: {
    width: 60,
    height: 60,
    backgroundColor: 'transparent',
  },
});

export default StackScreenHeader;
