/***************************************************************************************
 *    Title: react-native-user-profile
 *    Author: Nattatorn Yucharoen
 *    Date: 28th April, 2021
 *    Availability: https://github.com/nattatorn-dev/react-native-user-profile/blob/master/screens/Profile1/Email.js
 ***************************************************************************************/

import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

const FlatListItem = ({text, iconName}: {text: string; iconName: string}) => {
  console.log(text, iconName);
  return (
    <View style={styles.container}>
      <View style={styles.iconRow}>
        <Icon name={iconName} style={styles.flatListIcon} />
      </View>
      <View style={styles.flatListRow}>
        <View style={styles.flatListColumn}>
          <Text style={styles.flatListText}>{text}</Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    marginBottom: 25,
  },
  flatListColumn: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    marginBottom: 5,
  },
  flatListIcon: {
    color: 'gray',
    fontSize: 30,
  },
  emailNameColumn: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  emailNameText: {
    color: 'gray',
    fontSize: 14,
    fontWeight: '200',
  },
  flatListRow: {
    flex: 8,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  flatListText: {
    fontSize: 16,
  },
  iconRow: {
    flex: 4,
    alignItems: 'center',
  },
});

export default FlatListItem;
