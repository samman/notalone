import React, {
  createContext,
  useContext,
  useEffect,
  useRef,
  useState,
} from 'react';
import {getAllCommunities} from '../api/community';
import TCommunity from '../types/TCommunity';

import {ErrorContext} from './ErrorContext';
import {ParentDataContext} from './ParentDataContext';

type TCommunitiesContextValue = {
  communities: Array<TCommunity>;
  setCommunities: React.Dispatch<React.SetStateAction<Array<TCommunity>>>;
  isCommunitiesLoading: boolean;
  setIsCommunitiesLoading: React.Dispatch<React.SetStateAction<boolean>>;
  shouldLoadMore: boolean;
  setShouldLoadMore: React.Dispatch<React.SetStateAction<boolean>>;
  searchTerm: string;
  setSearchTerm: React.Dispatch<React.SetStateAction<string>>;
};

export const CommunitiesContext = createContext<TCommunitiesContextValue>({
  communities: [],
  setCommunities: () => {},
  isCommunitiesLoading: false,
  setIsCommunitiesLoading: () => {},
  shouldLoadMore: false,
  setShouldLoadMore: () => {},
  searchTerm: '',
  setSearchTerm: () => {},
});

export const CommunitiesContextProvider = (props: {children: any}) => {
  const [communities, setCommunities] = useState<Array<TCommunity>>([]);
  const [isCommunitiesLoading, setIsCommunitiesLoading] = useState<boolean>(
    false,
  );
  const page = useRef(1);
  const [limit] = useState(10);
  const [sort] = useState(null);
  const [fields] = useState(null);
  const [searchTerm, setSearchTerm] = useState('');
  const [shouldLoadMore, setShouldLoadMore] = useState(false);

  const {
    shouldCommunitiesDataUpdate,
    setShouldComunitiesDataUpdate,
  } = useContext(ParentDataContext);
  const {handleError} = useContext(ErrorContext);

  const fetchCommunitiesData = async () => {
    try {
      setIsCommunitiesLoading(true);
      const res = await getAllCommunities({searchTerm});
      setCommunities(res.data.communities);
    } catch (err) {
      handleError(err);
    } finally {
      setIsCommunitiesLoading(false);
      setShouldComunitiesDataUpdate(false);
    }
  };

  useEffect(() => {
    if (shouldCommunitiesDataUpdate) {
      fetchCommunitiesData();
      setShouldComunitiesDataUpdate(false);
    }
  }, [shouldCommunitiesDataUpdate]);

  useEffect(() => setShouldComunitiesDataUpdate(true), [searchTerm]);

  useEffect(() => {
    if (shouldLoadMore) {
      page.current = page.current + 1;
      const apiReqFunc = async () => {
        try {
          setIsCommunitiesLoading(true);
          const res = await getAllCommunities({
            page: page.current,
            limit,
            sort,
            fields,
            searchTerm,
          });
          setCommunities((prev) => [...prev, ...res.data.communities]);
        } catch (err) {
          handleError(err);
        } finally {
          setIsCommunitiesLoading(false);
        }
      };
      apiReqFunc();
      setShouldLoadMore(false);
    }
  }, [shouldLoadMore]);

  return (
    <CommunitiesContext.Provider
      value={{
        communities,
        setCommunities,
        isCommunitiesLoading,
        setIsCommunitiesLoading,
        shouldLoadMore,
        setShouldLoadMore,
        searchTerm,
        setSearchTerm,
      }}>
      {props.children}
    </CommunitiesContext.Provider>
  );
};
