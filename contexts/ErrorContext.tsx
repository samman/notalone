import React, {createContext, useState, useEffect} from 'react';

// Alert
import MessageAlert, {MessageAlertType} from '../utils/MessageAlert';

interface IError {
  error: string;
  handleError: (err: any) => void;
  mustLogout: boolean;
  setMustLogout: React.Dispatch<React.SetStateAction<boolean>>;
}

export const ErrorContext = createContext<IError>({
  error: '',
  handleError: () => {},
  mustLogout: false,
  setMustLogout: () => {},
});

export const ErrorContextProvider = (props: any) => {
  const [error, setError] = useState('');
  const [mustLogout, setMustLogout] = useState(false);

  useEffect(() => {
    if (error) {
      MessageAlert(error, MessageAlertType.ERROR);
      setError('');
    }
  }, [error]);

  const handleError = (err: any) => {
    if (err.response?.status === 401) {
      //logout
      setMustLogout(true);
    } else {
      setError(err.response?.data?.message);
    }
  };

  return (
    <ErrorContext.Provider value={{error, handleError, mustLogout, setMustLogout}}>
      {props.children}
    </ErrorContext.Provider>
  );
};
