import React, {
  createContext,
  useContext,
  useEffect,
  useRef,
  useState,
} from 'react';
import {getJoinedCommunityMeetups} from '../api/meetup';
import TMeetup from '../types/TMeetup';

import {ErrorContext} from './ErrorContext';
import {ParentDataContext} from './ParentDataContext';

type TMeetupsContextValue = {
  meetups: Array<TMeetup>;
  setMeetups: React.Dispatch<React.SetStateAction<Array<TMeetup>>>;
  isMeetupsLoading: boolean;
  setIsMeetupsLoading: React.Dispatch<React.SetStateAction<boolean>>;
  shouldMeetupsDataUpdate: boolean;
  setShouldMeetupsDataUpdate: React.Dispatch<React.SetStateAction<boolean>>;
  shouldLoadMore: boolean;
  setShouldLoadMore: React.Dispatch<React.SetStateAction<boolean>>;
};

export const MeetupsContext = createContext<TMeetupsContextValue>({
  meetups: [],
  setMeetups: () => {},
  isMeetupsLoading: false,
  setIsMeetupsLoading: () => {},
  shouldMeetupsDataUpdate: false,
  setShouldMeetupsDataUpdate: () => {},
  shouldLoadMore: false,
  setShouldLoadMore: () => {},
});

export const MeetupsContextProvider = (props: {children: any}) => {
  const [meetups, setMeetups] = useState<Array<TMeetup>>([]);
  const [isMeetupsLoading, setIsMeetupsLoading] = useState<boolean>(false);
  const page = useRef(0);
  const [shouldLoadMore, setShouldLoadMore] = useState(false);

  const {handleError} = useContext(ErrorContext);
  const {shouldMeetupsDataUpdate, setShouldMeetupsDataUpdate} = useContext(
    ParentDataContext,
  );

  const fetchMeetupsData = async () => {
    try {
      setIsMeetupsLoading(true);
      const res = await getJoinedCommunityMeetups({
        page,
      });
      setMeetups(res.data.meetups);
      setShouldMeetupsDataUpdate(false);
    } catch (err) {
      handleError(err);
    } finally {
      setIsMeetupsLoading(false);
    }
  };

  useEffect(() => {
    if (shouldMeetupsDataUpdate) {
      fetchMeetupsData();
    }
  }, [shouldMeetupsDataUpdate]);

  useEffect(() => {
    setShouldMeetupsDataUpdate(true);
  }, []);

  useEffect(() => {
    if (shouldLoadMore) {
      page.current = page.current + 1;
      const apiReqFunc = async () => {
        try {
          setIsMeetupsLoading(true);
          const res = await getJoinedCommunityMeetups({
            page: page.current,
          });
          setMeetups((prev) => [...prev, ...res.data.meetups]);
        } catch (err) {
          handleError(err);
        } finally {
          setIsMeetupsLoading(false);
        }
      };
      apiReqFunc();
      setShouldLoadMore(false);
    }
  }, [shouldLoadMore]);

  return (
    <MeetupsContext.Provider
      value={{
        meetups,
        setMeetups,
        isMeetupsLoading,
        setIsMeetupsLoading,
        shouldMeetupsDataUpdate,
        setShouldMeetupsDataUpdate,
        shouldLoadMore,
        setShouldLoadMore,
      }}>
      {props.children}
    </MeetupsContext.Provider>
  );
};
