import AsyncStorage from '@react-native-async-storage/async-storage';
import React, {createContext, useState, useContext, useEffect} from 'react';
import {getProfile} from '../api/profile';
import {ErrorContext} from './ErrorContext';

type TAuthContextProviderProps = {
  children: any;
};

type TAuthContextValues = {
  isLoggedIn: boolean;
  setIsLoggedIn: any;
  loggedInUser: TLoggedInUser | null;
  isProfileLoading: boolean;
  setIsProfileManipulated: any;
  jwtToken: String | null;
  setJwtToken: any;
  logout: ()=>void;
};

export type TLoggedInUser = {
  _id: string;
  name: string;
  username: string;
  email: string;
  profilePic: string;
  address: string;
  gender: Gender;
};

export enum Gender{
  male='male',
  female='female',
  other='other'
}

export const AuthContext = createContext<TAuthContextValues>({
  isLoggedIn: false,
  setIsLoggedIn: null,
  loggedInUser: null,
  isProfileLoading: false,
  setIsProfileManipulated: null,
  jwtToken: null,
  setJwtToken: null,
  logout: () =>{},
});

export const AuthContextProvider = (props: TAuthContextProviderProps) => {
  const [isLoggedIn, setIsLoggedIn] = useState<boolean>(false);
  const [loggedInUser, setIsLoggedInUser] = useState<TLoggedInUser | null>(
    null,
  );
  const [jwtToken, setJwtToken] = useState<String|null>(null);
  const [isProfileLoading, setIsProfileLoading] = useState<boolean>(false);
  const [isProfileManipulated, setIsProfileManipulated] = useState<boolean>(
    false,
  );
  const {handleError, mustLogout, setMustLogout} = useContext(ErrorContext);

  // Retreives the users profile whenever the profile is manipulated
  useEffect(() => {
    (async () => {
      try {
        if (isProfileManipulated) {
          setIsProfileLoading(true);
          const res = await getProfile();
          setIsLoggedInUser(res.data.profile);
        }
      } catch (err) {
        handleError(err);
      } finally {
        setIsProfileLoading(false);
        setIsProfileManipulated(false);
      }
    })();
  }, [isProfileManipulated]);

  // Sets isProfileManipulated to true when isLoggedIn is true
  useEffect(() => {
    if (isLoggedIn) {
      setIsProfileManipulated(true);
    } else {
      setIsLoggedInUser(null);
    }
  }, [isLoggedIn]);

  // toogles logged in to true when if jwtToken in set
  useEffect(() => {
    if (jwtToken) {
      setIsLoggedIn(true);
    } else {
      setIsLoggedIn(false);
    }
  }, [jwtToken]);

  // Checks for jwt when the user loads the apps
  useEffect(() => {
    (async () => {
      const token = await AsyncStorage.getItem('jwt_token');
      if (token) {
        setJwtToken(token);
      }
    })();
  }, []);

  // Trigger logout function
  useEffect(() => {
    if(mustLogout){
      logout();
      setMustLogout(false)
    }
  }, [mustLogout])

  // Logout function
  const logout = async() => {
    await AsyncStorage.removeItem('jwt_token');
    setJwtToken(null);
  }

  return (
    <AuthContext.Provider
      value={{
        isLoggedIn,
        setIsLoggedIn,
        loggedInUser,
        isProfileLoading,
        setIsProfileManipulated,
        jwtToken,
        setJwtToken,
        logout
      }}>
      {props.children}
    </AuthContext.Provider>
  );
};
