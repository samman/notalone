import React, {createContext, useState} from 'react';

type TParentDataContextValue = {
  shouldCommunitiesDataUpdate: boolean;
  setShouldComunitiesDataUpdate: React.Dispatch<React.SetStateAction<boolean>>;
  shouldPostsDataUpdate: boolean;
  setShouldPostsDataUpdate: React.Dispatch<React.SetStateAction<boolean>>;
  shouldMeetupsDataUpdate: boolean;
  setShouldMeetupsDataUpdate: React.Dispatch<React.SetStateAction<boolean>>;
};

export const ParentDataContext = createContext<TParentDataContextValue>({
  shouldCommunitiesDataUpdate: false,
  setShouldComunitiesDataUpdate: () => {},
  shouldPostsDataUpdate: false,
  setShouldPostsDataUpdate: () => {},
  shouldMeetupsDataUpdate: false,
  setShouldMeetupsDataUpdate: () => {},
});

export const ParentDataContextProvider = (props: {
  children: React.ReactNode;
}) => {
  const [
    shouldCommunitiesDataUpdate,
    setShouldComunitiesDataUpdate,
  ] = useState<boolean>(true);
  const [shouldPostsDataUpdate, setShouldPostsDataUpdate] = useState<boolean>(
    true,
  );
  const [
    shouldMeetupsDataUpdate,
    setShouldMeetupsDataUpdate,
  ] = useState<boolean>(true);

  return (
    <ParentDataContext.Provider
      value={{
        shouldCommunitiesDataUpdate,
        setShouldComunitiesDataUpdate,
        shouldPostsDataUpdate,
        setShouldPostsDataUpdate,
        shouldMeetupsDataUpdate,
        setShouldMeetupsDataUpdate,
      }}>
      {props.children}
    </ParentDataContext.Provider>
  );
};
