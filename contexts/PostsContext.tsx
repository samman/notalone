import React, {createContext, useContext, useEffect, useState} from 'react';
import {getJoinedCommunityPosts} from '../api/post';
import TPost from '../types/TPost';

import {ErrorContext} from './ErrorContext';
import {ParentDataContext} from './ParentDataContext';

type TPostsContextValue = {
  posts: Array<TPost>;
  setPosts: React.Dispatch<React.SetStateAction<Array<TPost>>>;
  isPostsLoading: boolean;
  setIsPostsLoading: React.Dispatch<React.SetStateAction<boolean>>;
  page: number;
  setPage: React.Dispatch<React.SetStateAction<number>>;
};

export const PostsContext = createContext<TPostsContextValue>({
  posts: [],
  setPosts: () => {},
  isPostsLoading: false,
  setIsPostsLoading: () => {},
  page: 0,
  setPage: () => {},
});

export const PostsContextProvider = (props: {children: any}) => {
  const [posts, setPosts] = useState<Array<TPost>>([]);
  const [isPostsLoading, setIsPostsLoading] = useState<boolean>(false);
  const [page, setPage] = useState(0);

  const {handleError} = useContext(ErrorContext);
  const {shouldPostsDataUpdate, setShouldPostsDataUpdate} = useContext(
    ParentDataContext,
  );

  const fetchPostsData = async () => {
    try {
      setIsPostsLoading(true);
      const res = await getJoinedCommunityPosts({
        page,
      });
      console.log(res.data.posts)
      setPosts(res.data.posts);
      setShouldPostsDataUpdate(false);
    } catch (err) {
      handleError(err);
    } finally {
      setIsPostsLoading(false);
    }
  };

  useEffect(() => {
    if (shouldPostsDataUpdate) {
      fetchPostsData();
    }
  }, [shouldPostsDataUpdate]);

  useEffect(() => {
    setShouldPostsDataUpdate(true);
  }, [page]);

  return (
    <PostsContext.Provider
      value={{
        posts,
        setPosts,
        isPostsLoading,
        setIsPostsLoading,
        page,
        setPage,
      }}>
      {props.children}
    </PostsContext.Provider>
  );
};
