const colorGradients = [
  ['#fcdf8a', '#f38381'],
  ['#42e695', '#3bb2b8'],
  ['#a5d9fe', '#0598ff'],
  ['#CE9FFC', '#7367F0'],
];

export {
    colorGradients
}